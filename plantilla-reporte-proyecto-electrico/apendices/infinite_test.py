#!/usr/bin/env python
# coding=utf-8
  
"""Paquetes importados"""
import rospy
import numpy as np
import pandas as pd
from geometry_msgs.msg import Pose2D

"""Habilita la escritura del archivo"""
WRITE = True
"""String para el nombre del archivo que guarda los datos"""
STR_NAME = 'infinite_test.csv'

class infinite_test():

    """
    Clase utilizada para implementar la prueba de un infinito para algoritmos de
    seguimiento de trayectoria.

    Atributos:
    ---------
    rospy.init_node('square_test_node', anonymous=False): Instancia del objeto init_node de Rospy
        Inicia el nodo con su respectivo nombre.
    data_list: Python list
        Lista utilizada para almacenar los datos que se va exportar.
    point_pub: Instancia del objeto Publisher de Rospy
        Objeto que publica el punto a seguir en su respectivo topico.

    Metodos:
    -------
    __init__:
        Constructor de la clase.
    infinite:
        Ejecuta la prueba.
    point_publisher:
        Publica los puntos a seguir.
    """

    def __init__(self):
        """
        Inicializa los atributos de la clase.

        Parametros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        rospy.init_node('infinite_test_node', anonymous=False)
        self.data_list = list()
        self.point_pub = rospy.Publisher("desired_point", Pose2D, queue_size=10) 

    def infinite(self):
        """
        Ejecuta la prueba y exporta los datos.

        Parametros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        while not rospy.is_shutdown():
            seconds = rospy.get_time()
            d = 6
            t = 0.25 * seconds
            x = d * np.sqrt(2) * np.cos(t) / (np.power(np.sin(t), 2) + 1)
            y = d * np.sqrt(2) * np.cos(t) * np.sin(t) / (np.power(np.sin(t), 2) + 1)
            if (WRITE == True):
                results = [str(x), str(y)]
                self.data_list.append(results)
                frame = pd.DataFrame(self.data_list)
                frame.to_csv(STR_NAME) 
            self.point_publisher(x,y)
    
    def point_publisher(self, x, y):
        """
        Publica los puntos.

        Parametros:
        ----------
            x: float
                Coordenada x del punto 
            y: float
                Coordenada y del punto
        Returns:
        --------
            Ninguno
        """
        msg = Pose2D()
        msg.x = x
        msg.y = y
        self.point_pub.publish(msg)

def main():
    test = infinite_test()
    test.infinite()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass