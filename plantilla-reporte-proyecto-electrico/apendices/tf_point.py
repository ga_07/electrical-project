#!/usr/bin/env python

"""Paquetes importados"""  
import tf
import rospy
from geometry_msgs.msg import Pose2D

class transform_point():

    """
    Clase utilizada para implementar la transformacion de los puntos de una trayectoria
    a un marco de referencia global.

    Atributos:
    ---------
    rospy.init_node('square_test_node', anonymous=False): Instancia del objeto init_node de Rospy
        Inicia el nodo con su respectivo nombre.
    x_tf: float
        Coordenada x del putno a transformar.
    y_tf: float
        Coordenada y del punto a transformar.
    point_sub: Instancia del objeto Suscriber de Rospy
        Objeto que se suscribe al topico con los puntos a transformar.

    Metodos:
    -------
    __init__:
        Constructor de la clase.
    tf_broadcaster:
        Ejecuta la transformacion.
    point_callback:
        Publica los puntos a seguir.
    """

    def __init__(self):
        """
        Inicializa los atributos de la clase.

        Parametros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        rospy.init_node("tf_point", anonymous=False)
        self.x_tf = 0
        self.y_tf = 0
        self.point_sub = rospy.Subscriber("desired_point", Pose2D, self.point_callback, queue_size=100)

    def tf_broadcaster(self):
        """
        Ejecuta la transformacion de los puntos de la trayectoria.

        Parametros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        rate = rospy.Rate(100)
        br = tf.TransformBroadcaster()
        while not rospy.is_shutdown():
            br.sendTransform((self.x_tf, self.y_tf, 0),
                         tf.transformations.quaternion_from_euler(0, 0, 1.0),
                         rospy.Time.now(),
                         "point",
                         "odom")
            rate.sleep()

    def point_callback(self, msg):
        """
        Suscriptor que obtiene los puntos a transformar.

        Parametros:
        ----------
            x: float
                Coordenada x del punto 
            y: float
                Coordenada y del punto
        Returns:
        --------
            Ninguno
        """
        self.x_tf = msg.x
        self.y_tf = msg.y

def main():
    tf = transform_point()
    tf.tf_broadcaster()
    rospy.spin()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass