#!/usr/bin/env python
# coding=utf-8

"""Paquetes importados"""
import tf
import rospy
import numpy as np
import pandas as pd
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from rosgraph_msgs.msg import Clock
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import JointState

""" *** Constantes *** """
"""Periodo de muestreo"""
TS = 0.01
"""Distancia entre las ruedas del robot"""
DIST_b = 0.507
"""Distancia e desde el eje de traccion del robot"""
DIST_e = DIST_b * 2
"""Radio de las ruedas"""
WHEEL_R = 0.13
"""Limite"""
CMD_EPSILUM = 3
"""Indice de x"""
X_POS = 0
"""Indice de y"""
Y_POS = 1
"""Indice de theta"""
THETA = 2
"""Velocidad deseada"""
DES_V = 0.5
"""Habilita la escritura del archivo"""
WRITE = True
"""String para el nombre del archivo que guarda los datos"""
STR_NAME = 'btg_doc_stddev1_5.csv'

class braintenberg():

    """
    Clase utilizada para implementar el algoritmo de punto descentralizado para seguimiento
    de trayectorias.

    Atributos:
    ---------
    rospy.init_node('braintenberg_node', anonymous=False): Instancia del objeto init_node de Rospy
        Inicia el nodo con su respectivo nombre.
    Ts: float
        Periodo de muestreo
    x_n: float
        Posicion en el instante actual de la rueda derecha.
    x_n_1: float
        Posicion en el instante anterior de la rueda derecha.
    y_n: float
        Posicion en el instante actual de la rueda izquierda.
    y_n_1: float
        Posicion en el instante anterior de la rueda izquierda.
    wxk_1: float
        Velocidad angular en el instante anterior de la rueda derecha.
    wyk_1: float
        Velocidad angular en el instante anterior de la rueda izquierda.
    vxk_1: float
        Velocidad lineal en el instante anterior de la rueda derecha.
    vyk_1: float
        Velocidad lineal en el instante anterior de la rueda izquierda.
    Lk: NumPy ndarray
        Array que contiene los valores actuales de odometria del simulador.
    my_Lk: NumPy ndarray
        Array que contiene los valores actuales de odometria a partir de las ecuaciones.
    my_Lk_1: NumPy ndarray
        Array que contiene los valores anteriores de odometria a partir de las ecuaciones.
    data_list: Python list
        Lista utilizada para almacenar los datos que se va exportar.
    time: Instancia del objeto Time de Rospy
        Contiene el tiempo actual dado por ROS.
    tk: float
        Instante de tiempo actual.
    tk_1: float
        Instante de tiempo anterior.
    cmd_vel_pub: Instancia del objeto Publisher de Rospy
        Objeto que publica las velocidades del robot en su respectivo topico.
    odom_vel_sub: Instancia del objeto Suscriber de Rospy
        Objeto que se suscribe al topico de la odometria del simulador
    clk_sub: Instancia del objeto Suscriber de Rospy
        Objeto que se suscribe al topico de tiempo del simulador
    joint_sub: Instancia del objeto Suscriber de Rospy
        Objeto que se suscribe al topico de estado de las articulaciones.

    Metodos:
    -------
    __init__:
        Constructor de la clase.
    odom_callback:
        Funcion callback que obtiene la odometria del simulador.
    lidar_callback:
        Funcion callback que obtiene la lectura del sensor.
    inverse_kinematic_model:
        Calcula las velocidades de las ruedas en el marco de referencia local.
    direct_kinematics:
        Calcula la odometria del robot.
    cmd_vel_publisher:
        Publica las velocidades de las ruedas durante el ciclo de control.
    clock_callback:
        Funcion callback que obtiene el tiempo de la simulacion.
    set_up_time:
        Calcula el periodo de muestreo en cada ciclo y lo actualiza.
    joint_callback:
        Funcion callback que obtiene el de posicion de las ruedas.
    run:
        Ejecuta todo el algoritmo y tambien escucha la transformada del punto de referencia.
    """

    def __init__(self):
        """
        Inicializa los atributos de la clase

        Parametros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        rospy.init_node('braintenberg_node', anonymous=False)
        self.right_sensor = 0
        self.left_sensor = 0
        self.infinity = float('inf') 
        self.ranges = []
        self.vx = 0.0
        self.vy = 0.0
        self.Ts = TS
        self.x_n = 0.0
        self.x_n_1 = 0.0
        self.y_n = 0.0
        self.y_n_1 = 0.0
        self.wxk_1 = 0.0
        self.wyk_1 = 0.0
        self.vxk_1 = 0.0
        self.vyk_1 = 0.0
        self.wk_1 = 0.0
        self.Lk = np.zeros(3)
        self.my_Lk = np.zeros(3) 
        self.my_Lk_1 = np.zeros(3)
        self.data_list = list()
        self.time = rospy.Time.now()
        self.tk = 0.0
        self.tk_1 = 0.0
        self.cmd_vel_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=100)
        self.odom_vel_sub = rospy.Subscriber("odom", Odometry, self.odom_callback, queue_size=100)
        self.clk_sub = rospy.Subscriber("clock", Clock, self.clock_callback, queue_size=100)
        self.joint_sub = rospy.Subscriber("joint_states", JointState, self.joint_callback, queue_size=100)
        self.lidar_sub = rospy.Subscriber("lidar_1/scan_filtered", LaserScan, self.lidar_callback, queue_size=100)

    def odom_callback(self, msg):
        """
        Suscriptor de la odometria.
        orientation_list: Lista para la transformacion de euler a quaternion.

        Parametros:
        -----------
            msg: Mensaje del tipo Odometry.
        Returns:
        --------
            Ninguno
        """
        orientation_list = [msg.pose.pose.orientation.x, msg.pose.pose.orientation.y,
                            msg.pose.pose.orientation.z, msg.pose.pose.orientation.w]
        (roll, pitch, theta) = tf.transformations.euler_from_quaternion(orientation_list)
        self.Lk[X_POS] = msg.pose.pose.position.x
        self.Lk[Y_POS] = msg.pose.pose.position.y
        self.Lk[THETA] = theta

    def lidar_callback(self, msg):
        """
        Suscriptor de las lecturas del sensor.

        Parametros:
        ----------
            msg:
                Mensaje del tipo LaserScan
        Returns:
        --------
            Ninguno
        """
        self.ranges = msg.ranges
        half = (len(self.ranges)/2)
        right_readings = 0
        left_readings = 0
        for i in range(len(self.ranges)):
            if (i < half):
                if (self.ranges[i] != self.infinity):
                    self.left_sensor += self.ranges[i]   
                    right_readings += 1
            else:
                if (self.ranges[i] != self.infinity):
                    self.right_sensor += self.ranges[i]
                    left_readings += 1
        self.right_sensor = float(self.right_sensor/right_readings)
        self.left_sensor = float(self.left_sensor/left_readings)  

    def inverse_kinematic_model(self, vxp_vyp):
        """
        Ecuaciones:

          B = | e*cos(THETA) + 0.5*c*sen(THETA)     e*sen(THETA) - 0.5c*cos(THETA) |
        (2x2) | e*cos(THETA) - 0.5*c*sen(THETA)     e*sen(THETA) + 0.5c*cos(THETA) |

        C = (1/e) * B

        => C = (1/e) * | e*cos(THETA) + 0.5*c*sen(THETA)     e*sen(THETA) - 0.5*c*cos(THETA) |
                       | e*cos(THETA) - 0.5*c*sen(THETA)     e*sen(THETA) + 0.5*c*cos(THETA) |
        
        vR_vL = C * vxp_vyp

        => vR_vL = (1/e) * | e*cos(THETA) - 0.5*c*sen(THETA)     e*sen(THETA) + 0.5*c*cos(THETA) | * | vxp |
                           | e*cos(THETA) + 0.5*c*sen(THETA)     e*sen(THETA) - 0.5*c*cos(THETA) |   | vyp |  
                
        Parametros:
        ----------
            vxp_vyp: NumPy ndarray
                Array que contiene las velocidades de las ruedas en un marco global.
        Returns:
        --------
            vxp_vyp: NumPy ndarray
                Array que contiene las velocidades de las ruedas en un marco local.
        """
        B = np.array([
            [(DIST_e * np.cos(self.Lk[THETA])) - (0.5 * DIST_b * np.sin(self.Lk[THETA])),
             (DIST_e * np.sin(self.Lk[THETA])) + (0.5 * DIST_b * np.cos(self.Lk[THETA]))],
            [(DIST_e * np.cos(self.Lk[THETA])) + (0.5 * DIST_b * np.sin(self.Lk[THETA])),
             (DIST_e * np.sin(self.Lk[THETA])) - (0.5 * DIST_b * np.cos(self.Lk[THETA]))]
        ])
        inverse_e = float(1/DIST_e)
        C = inverse_e * B
        vR_vL = np.dot(C, vxp_vyp)
        return vR_vL

    def direct_kinematics(self, vr, vl):
        """
        Ecuaciones:

          D = | vk-1 * cos(THETA_k-1) |
        (3x1) | vk-1 * sen(THETA_k-1) |
              |         wk-1          |

        E = Ts * D

        => E = Ts * | vk-1 * cos(THETA_k-1) |
         (3x1)      | vk-1 * sen(THETA_k-1) |
                    |         wk-1          |

        Lk = Lk-1 + E

        => Lk = |    xk-1   |    + Ts * | vk-1 * cos(THETA_k-1) |
         (3x1)  |    yk-1   |    +      | vk-1 * sen(THETA_k-1) |
                | THETA_k-1 |    +      |         wk-1          |   
        
        vk_1: Velocidad lineal del robot
        wk_1: Velocidad lineal del robot

        vk_1 = (vr + vl)/2
        wk_1 = (vr - vl)/b

        Parametros:
        ----------
            vr: float
                Velocidad de la rueda derecha en un marco local.
            vl: float
                Velocidad de la rueda izquierda en un marco local.
        Returns:
        --------
            Ninguno   
        """
        my_vk_1 = float((self.vxk_1 + self.vyk_1)/2) 
        self.wk_1 = float((self.vxk_1 - self.vyk_1)/DIST_b)
        self.my_Lk_1 = self.my_Lk
        D = np.array([
            my_vk_1 * np.cos(self.my_Lk_1[THETA]),
            my_vk_1 * np.sin(self.my_Lk_1[THETA]),
            self.wk_1
        ])
        self.set_up_time()
        E = self.Ts * D
        self.my_Lk = np.add(self.my_Lk_1, E)

    def cmd_vel_publisher(self):
        """
        Publica las velocidades de las ruedas segun la lectura del sensor.

        Parametros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        msg = Twist()
        if ((self.right_sensor < self.left_sensor) and (self.right_sensor < CMD_EPSILUM)):
                self.vx = DES_V * 2
                self.vy = DES_V
        elif ((self.right_sensor > self.left_sensor) and (self.left_sensor < CMD_EPSILUM)):
                self.vx = DES_V
                self.vy = DES_V * 2
        else:
            self.vx = DES_V
            self.vy = DES_V 
        vxp_vyp = [self.vx, self.vy]
        vd_vi = self.inverse_kinematic_model(vxp_vyp)
        self.direct_kinematics(vd_vi[0], vd_vi[1])
        vk_1 = float((self.vx + self.vy)/2)
        wk_1 = float((self.vx - self.vy)/DIST_b)
        msg.linear.x = vk_1
        msg.linear.y = 0.0
        msg.linear.z = 0.0
        msg.angular.x = 0.0
        msg.angular.y = 0.0
        msg.angular.z = wk_1
        self.cmd_vel_pub.publish(msg)

    def clock_callback(self, msg):
        """
        Parametros:
        ----------
            msg:
                Mensaje del tipo Clock
        Returns:
        --------
            Ninguno
        """
        self.tk = msg.clock.to_sec()

    def set_up_time(self):
        """
        Ecuacion:

        Ts = tk - (tk-1)

        Parametros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        self.Ts = self.tk - self.tk_1
        self.tk_1 = self.tk

    def joint_callback(self, msg):
        """
        Parametros:
        ----------
            msg:
                Mensaje del tipo JointState
        Returns:
        --------
            Ninguno
        """
        if (self.Ts == 0):
            self.Ts = TS
        if((msg.position[0] != 0) and (msg.position[1] != 0)):
            self.x_n = msg.position[0]
            self.y_n = msg.position[1]
        self.wxk_1 = (self.x_n - self.x_n_1)/self.Ts
        self.wyk_1 = (self.y_n - self.y_n_1)/self.Ts
        self.vxk_1 = WHEEL_R * self.wxk_1
        self.vyk_1 = WHEEL_R * self.wyk_1
        self.x_n_1 = self.x_n
        self.y_n_1 = self.y_n

    def run(self):
        """
        Ejecuta la simulacion y exporta los datos.

        Parametros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            if (WRITE == True):
                results = [str(self.Lk[0]), str(self.Lk[1]), str(self.Lk[2]), str(self.tk)]
                self.data_list.append(results)
                frame = pd.DataFrame(self.data_list)
                frame.to_csv(STR_NAME)            
            self.cmd_vel_publisher()
            rate.sleep()

def main():
    braintenberg_ob = braintenberg()
    braintenberg_ob.run()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass


    