\changetocdepth {2}
\babel@toc {spanish}{}
\contentsline {chapter}{\'{I}ndice general}{xi}{section*.1}
\contentsline {chapter}{\'{I}ndice de figuras}{xiii}{section*.2}
\contentsline {chapter}{\'{I}ndice de tablas}{xiv}{section*.3}
\contentsline {chapter}{\chapternumberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Alcances del proyecto}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Justificaci\IeC {\'o}n}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Objetivo general}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Objetivos espec\IeC {\'\i }ficos}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Metodolog\IeC {\'\i }a}{3}{section.1.5}
\contentsline {section}{\numberline {1.6}Organizaci\IeC {\'o}n del documento}{3}{section.1.6}
\contentsline {chapter}{\chapternumberline {2}Marco Te\IeC {\'o}rico}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Navegaci\IeC {\'o}n de robots aut\IeC {\'o}nomos}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Robot m\IeC {\'o}vil en configuraci\IeC {\'o}n diferencial}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Modelado}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Marcos de referencia y transformaciones}{8}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Algoritmos de seguimiento de trayectoria}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Algoritmo de persecuci\IeC {\'o}n pura}{9}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Algoritmo de punto desentralizado}{11}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Algoritmos de evasi\IeC {\'o}n de obst\IeC {\'a}culos}{13}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Veh\IeC {\'\i }culos de Braintenberg}{13}{subsection.2.4.1}
\contentsline {subsubsection}{Moverse (veh\IeC {\'\i }culo 1)}{13}{section*.13}
\contentsline {subsubsection}{Miedo y agresi\IeC {\'o}n (veh\IeC {\'\i }culo 2)}{14}{section*.16}
\contentsline {subsubsection}{Amor (veh\IeC {\'\i }culo 3)}{17}{section*.21}
\contentsline {chapter}{\chapternumberline {3}Herramientas utilizadas}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Software de simulaci\IeC {\'o}n rob\IeC {\'o}tica}{19}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Robotic Operating System (ROS)}{19}{subsection.3.1.1}
\contentsline {subsubsection}{Nivel de sistema de archivos}{20}{section*.27}
\contentsline {subsubsection}{Nivel gr\IeC {\'a}fico de c\IeC {\'a}lculo}{20}{section*.28}
\contentsline {subsubsection}{Nivel de comunidad}{21}{section*.29}
\contentsline {subsection}{\numberline {3.1.2}Gazebo}{22}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Integraci\IeC {\'o}n ROS y Gazebo}{23}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}El robot MP-500}{25}{section.3.2}
\contentsline {chapter}{\chapternumberline {4}Algoritmos implementados}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Generalidades}{27}{section.4.1}
\contentsline {section}{\numberline {4.2}Algoritmos de seguimiento de trayectoria}{28}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Punto descentralizado}{28}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Persecuci\IeC {\'o}n pura}{30}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Algoritmos de evasi\IeC {\'o}n de obst\IeC {\'a}culos}{32}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Veh\IeC {\'\i }culo de Braintenberg 2a: Miedo}{32}{subsection.4.3.1}
\contentsline {chapter}{\chapternumberline {5}Simulaci\IeC {\'o}n de los algoritmos implementados}{35}{chapter.5}
\contentsline {section}{\numberline {5.1}Generalidades de la simulaci\IeC {\'o}n}{35}{section.5.1}
\contentsline {section}{\numberline {5.2}Pruebas realizadas}{35}{section.5.2}
\contentsline {section}{\numberline {5.3}Resultados la simulaci\IeC {\'o}n de los algoritmos de seguimiento de trayectoria}{37}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Punto descentralizado}{37}{subsection.5.3.1}
\contentsline {subsubsection}{Trayectoria cuadrada}{37}{section*.46}
\contentsline {subsubsection}{Trayectoria circular}{39}{section*.50}
\contentsline {subsubsection}{Trayectoria de infinito}{40}{section*.54}
\contentsline {subsection}{\numberline {5.3.2}Persecuci\IeC {\'o}n pura}{42}{subsection.5.3.2}
\contentsline {subsubsection}{Trayectoria cuadrada}{42}{section*.58}
\contentsline {subsubsection}{Trayectoria circular}{44}{section*.62}
\contentsline {subsubsection}{Trayectoria de infinito}{45}{section*.66}
\contentsline {section}{\numberline {5.4}Resultado de la simulaci\IeC {\'o}n del algorito de evasi\IeC {\'o}n de obst\IeC {\'a}culos}{47}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Veh\IeC {\'\i }culo de Braintenberg 2a}{47}{subsection.5.4.1}
\contentsline {subsubsection}{Algoritmo de Braintenberg en la pista 2}{47}{section*.70}
\contentsline {subsubsection}{Algoritmo de Braintenberg en la pista 3}{49}{section*.75}
\contentsline {section}{\numberline {5.5}Resumen de los resultados obtenidos}{51}{section.5.5}
\contentsline {chapter}{\chapternumberline {6}Conclusiones y recomendaciones}{53}{chapter.6}
\contentsline {section}{\numberline {6.1}Conclusiones}{53}{section.6.1}
\contentsline {section}{\numberline {6.2}Recomendaciones}{54}{section.6.2}
\contentsline {section}{\numberline {6.3}Trabajos futuros}{54}{section.6.3}
\contentsline {appendix}{\chapternumberline {A}Implementaci\IeC {\'o}n del algoritmo de Punto Descentralizado}{55}{appendix.Alph1}
\contentsline {appendix}{\chapternumberline {B}Implementaci\IeC {\'o}n del algoritmo de Persecuci\IeC {\'o}n Pura}{65}{appendix.Alph2}
\contentsline {appendix}{\chapternumberline {C}Implementaci\IeC {\'o}n del algoritmo de Braintenberg 2a}{77}{appendix.Alph3}
\contentsline {appendix}{\chapternumberline {D}Implementaci\IeC {\'o}n de las pruebas para seguimiento de trayectoria}{87}{appendix.Alph4}
\contentsline {section}{\numberline {D.1}Prueba de trayectoria cuadrada}{87}{section.Alph4.1}
\contentsline {section}{\numberline {D.2}Prueba de trayectoria circular}{90}{section.Alph4.2}
\contentsline {section}{\numberline {D.3}Prueba de trayectoria de infinito}{92}{section.Alph4.3}
\contentsline {appendix}{\chapternumberline {E}Implementaci\IeC {\'o}n de la transformada de los puntos a un marco de referencia global}{97}{appendix.Alph5}
\contentsline {chapter}{Bibliograf\'{\i }a}{101}{section*.82}
