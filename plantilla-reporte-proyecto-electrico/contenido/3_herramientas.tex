% --------------------
  \chapter{Herramientas utilizadas}
% --------------------
\label{C:desarrollo}

\section{Software de simulación robótica}

\subsection{Robotic Operating System (ROS)}
\label{ros}

ROS, de las siglas en inglés ``Robotic Operating System'', es un meta sistema operativo Open Source que se utiliza como framework para la escritura de software para robots, debido a la flexibilidad que ofrece. Se le llama meta sistema operativo, o middleware, ya que no es un sistema operativo en el sentido tradicional de gestión y programación de procesos, sino que más bien proporciona una capa de comunicaciones estructurada por encima de los sistemas operativos host de un cluster de cómputo heterogéneo. Al mismo tiempo, proporciona los servicios que se espera de un sistema operativo tales como la abstracción de hardware, control de dispositivos a bajo nivel, implementación de funciones de uso común, transmisión de mensajes entre procesos y administración de paquetes; también proporciona bibliotecas para obtener, construir, escribir y ejecutar código en múltiples computadoras. \cite{inproceedings, ros1, ros2}

Entre los principales beneficios que ofrece ROS está la independencia del lenguaje de programación, ya que se ha logrado implementar con lenguajes de programación de alto nivel. Actualmente ha sido integrado en Python, C++ y Lisp, aunque la desventaja que presenta es que no se encuentra disponible para todas las plataformas, sólo para las que están basadas en sistemas Unix. Para la realización de este proyecto se trabajó con Python, por la facilidad y rapidez de prototipado que ofrece al ser un lenguaje interpretado de alto nivel; explícitamente se utilizó Rospy, que corresponde a una biblioteca cliente escrita en Python para ROS. \cite{ros1, ros3}

El funcionamiento de ROS se basa en la comunicación síncrona sobre servicios, la comunicación asíncrona de datos sobre tópicos y el almacenamiento de datos en un servidor de parámetros. Para implementarlo utiliza tres niveles de conceptos; el nivel de sistema de archivos, el nivel gráfico de cálculo y el nivel de comunidad, cada uno de estos niveles con sus propios conceptos \cite{ros4}. En la figura \ref{F:conceptos_ros} se muestra un diagrama del funcionamiento básico de los conceptos de ROS.

\begin{figure}[H]
\centering
\includegraphics[width=0.35\textwidth]{./imagenes/ROS_basic_concepts.png} 
\caption{Funcionamiento de los conceptos básicos de ROS. \cite{ros4}}
\label{F:conceptos_ros}
\end{figure}

\subsubsection{Nivel de sistema de archivos}

Los conceptos del nivel de sistema de archivos de ROS cubren principalmente los recursos que se encuentran en el disco:

\begin{itemize}
	\item \textbf{Paquetes}: Son la unidad principal para oganizar el software de ROS; puede contener procesos en tiempo de ejecución (nodos), una biblioteca que depende de ROS, conjuntos de datos, archivos de configuración o cualquier otra cosa que se organice de manera útil en conjunto. Los paquetes son los elementos más pequeños que se pueden ejecutar en ROS.
	\item \textbf{Meta-paquetes}: Son paquetes especializados que se utilizan para representar un grupo de otros paquetes relacionados.
	\item \textbf{Manifiesto del paquete}: Proporciona datos sobre el paquete como su nombre, su versión, su descripción, su información de licencia, sus dependencias y si utiliza meta-paquetes importados.
	\item \textbf{Repositorios}: Corresponde a una colección de paquetes que comparten un sistema de control de versiones (VCS) común. Los paquetes que comparten un VCS comparten la misma versión y se pueden lanzar juntos. También pueden contener sólo un paquete.
	\item \textbf{Tipos de mensajes}: Son las descripciones que definen las estructuras de datos para los mensajes enviados en ROS.
	\item \textbf{Tipos de servicios}: Son las descripciones que definen las estructuras de datos de solicitud y respuestas para los servicios en ROS.
\end{itemize}

\subsubsection{Nivel gráfico de cálculo}

Este nivel corresponde a la red peer-to-peer (potencialmente distribuida entre máquinas) de procesos de ROS donde se procesan los datos. Los conceptos de este nivel proporcionan datos de diferentes maneras.

\begin{itemize}
	\item \textbf{Nodos}: Son procesos que realizan cálculos, un sistema de control de robot generalmente comprende varios nodos. Proporcionan una vista gráfica del sistema; se escriben mediante el uso de una biblioteca cliente de ROS.
	\item \textbf{Maestro}: Es el nodo maestro, proporciona registro de nombres y realiza una búsqueda del resto de nodos. Sin el maestro, los nodos no pueden encontrarse, intercambiar mensajes o invocar servicios.
	\item \textbf{Servidor de parámetros}: Permite almacenar datos por clave en una ubicación central. Forma parte del maestro.
	\item \textbf{Mensajes}: Los nodos se comunican entre sí utilizando mensajes. Son estructuras de datos que comprenden campos escritos; se admiten los tipos de datos primitivos estándar así como matrices de estos mismos tipos. Además, pueden incluir estructuras y matrices anidadas de forma arbitraria.
	\item \textbf{Tópicos}: Los mensajes se enrutan a través de un sistema de publicación/subscripción; un nodo envían un mensaje publicándolo en un determinado tópico, es un nombre que se utiliza para identificar el contenido del mensaje. Así, un nodo se suscribe a un tópico para escuchar los mensajes que otro nodo publica. Puede haber varios suscriptores concurrentes para un solo tópico, así como que un nodo se suscriba a varios tópicos.
	\item \textbf{Servicios}: Utilizan un modelo de solicitud/respuesta; están definidos por dos estructuras de mensajes, una para la solicitud y otro para la respuesta. Un nodo proveedor ofrece un servicio con un nombre y un cliente utiliza el servicio enviando el mensaje de solicitud y esperando la respuesta. Las bibliotecas cliente de ROS generalmente presentan esta interacción al programador como si fuera una llamada a procedimiento remoto
	\item \textbf{Bolsas}: Es un formato para guardar y reproducir datos de mensajes de ROS, son un mecanismo para almacenar datos.
\end{itemize}

\subsubsection{Nivel de comunidad}

Los conceptos de este nivel se basan en recursos a través de los cuales las comunidades tienen la posibilidad de intercambiar software y conocimiento; estos recursos incluyen:

\begin{itemize}
	\item \textbf{Distribuciones}: Juegan un papel similar a las distribuciones de Linux, facilitan la instalación de una colección de software y también mantienen versiones consistentes.
	\item \textbf{Repositorios}: ROS se basa en una red federada de repositorios de código, en la cual diferentes instituciones pueden desarrollar y lanzar sus propios componentes de software para robots.
	\item \textbf{ROS Wiki}: El Wiki de la comunidad es el foro principal para documentar información sobre ROS. Cualquiera puede registrarse para obtener una cuenta y contribuir con su propia documentación, proporcionar correcciones o actualizaciones y escribir tutoriales.
\end{itemize}

\subsection{Gazebo}

Gazebo es un software de simulación robótica Open Source que ofrece la capacidad de simular de forma precisa y eficiente poblaciones de robots en entornos complejos tanto de interiores como de exteriores. Cuenta con un motor de física lo suficientemente eficiente para acercarse a la realidad, así como interfaces gráficas, como se muestra en la figura \ref{F:gazebo_sim}, e interfaces programáticas \cite{gazebo1}. Al igual que ROS, posee una comunidad de usuarios activa que contribuye aportando software.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{./imagenes/gazebo_simulation.png} 
\caption{Interfaz gráfica del software de simulación Gazebo.}
\label{F:gazebo_sim}
\end{figure}

Las principales características que posee Gazebo son:

\begin{itemize}
	\item \textbf{Simulación dinámica}: Utiliza motores de física de alto rendimiento, incluidos ODE (Open Dynamics Engine), Bullet Physics, Simbody: Multibody Physics API, y DART (Dynamic Animation and Robotics Toolkit).
	\item \textbf{Gráficos 3D}: Utilizando OGRE (motor de renderizado gráfico), Gazebo es capaz de proveer un renderzado de entornos realistas con detalles como luz, sombras y texturas.
	\item \textbf{Sensores y ruido}: Gazebo posee la capacidad de generar datos a partir del uso de sensores, a los cuales opcionalmente se les puede introducir ruido. Pueden simularse telémetros láser, cámaras 2D/3D, sensores de estilo Kinect, sensores de contacto, de fuerza, etc.
	\item \textbf{Plugins}: Permite el desarrollo de plugins personalizados para robots, sensores y control ambiental. Los plugins en Gazebo brindan acceso directo a su API (Application Programmable Interface).
	\item \textbf{Modelos de robots}: Se proporcionan distintos modelos de robots, pero también existe la posibilidad de crear un modelo personalizado utilizando SDF (Simulation Description Format). Este es un forma XML que describe objetos y entornos para simuladores, visualización y control de robots; se desarrolló teniendo en cuenta las aplicaciones científicas de los robots.
	\item \textbf{Herramientas de línea de comando}: Gazebo dispone de herramientas de línea de comandos para facilitar la introspección y el control de la simulación..
\end{itemize}

Además, en \cite{gazebo2} se presentan cuales son los componentes involucrados a la hora de ejecutar una simulación en Gazebo.

\begin{itemize}
	\item \textbf{Archivos World}: Estos archivos contienen la descripción de todos los elementos en la simulación, incluidos robots, luces, sensores y objetos estáticos. Estos archivos se crean utilizando SDF, y poseen .world como extensión.
	\item \textbf{Archivos de modelos}: Utilizan el mismo formato SDF que los archivos world, pero sólo contienen la opción <model> $\cdot \cdot \cdot$ </model>. El propósito de estos archivos es facilitar la reutilización del modelo y simplificar los archivos world. Una vez que estos archivos estén creados se pueden incluir en un archivos world.
	\item \textbf{Variables de ambiente}: Gazebo utiliza variables de ambiente para localizar archivos y configurar las comunicaciones entre el servidor y los clientes.
	\item \textbf{Servidor de Gazebo}: Es similar al corazón de Gazebo; analiza un archivo world con la descripción de un mundo proporcionado a través de línea de comandos, para luego simularlo utilizando el motor de física.
	\item \textbf{Cliente gráfico en conjunto con el servidor}: El cliente gráfico se conecta a un servidor en ejecución y visualiza los elementos, además de que permite modificar la simulación en ejecución. El comando gazebo combina el servidor y el cliente en un ejecutable.
	\item \textbf{Plugins}: Proporcionan un mecanismo simple y conveniente para interactuar con Gazebo, pueden cargarse en la línea de comando o especificarse en un archivo SDF.
\end{itemize}

\subsection{Integración ROS y Gazebo}
\label{gazebo_y_ros}

Existe la posibilidad de realizar simulaciones de software de robótica escrito mediante el uso de las bibliotecas clientes de ROS en Gazebo. Para que esto sea posible se necesita el conjunto de paquetes de ROS llamado gazebo\_ros\_pkgs, que proporcionan las interfaces necesarias para simular un robot en Gazebo utilizando mensajes, servicios y reconfiguración dinámica de ROS. En la figura \ref{F:gazebo_ros} se puede observar un diagrama de la interfaz de los paquetes de ROS que permiten la comunicación con Gazebo. Entre las características que ofrece estos paquetes se encuentran que admite una dependencia del sistema que es independiente de Gazebo, ya que no posee enlaces de ROS por sí sólo. También reduce la duplicación de código, mejora el soporte para los controladores que utilizan el paquete ros\_control, integra mejoras en la eficiencia del controlador en tiempo real y limpia el código antiguo de versiones anteriores de ROS y Gazebo \cite{gazebo3, ros5}.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{./imagenes/gazebo_ros.png} 
\caption{Diagrama de la interfaz del paquete gazebo\_ros\_pkgs. \cite{gazebo3}}
\label{F:gazebo_ros}
\end{figure}

Cuando se inicia el paquete de complementos de ROS, Gazebo es iniciado como un nodo llamado ``gazebo'' \cite{ros6}, se soportan las siguientes interfaces:

\begin{itemize}
	\item Tópicos suscritos a Gazebo
		\subitem \textbf{$\sim$/set\_link\_state}: Utiliza el tipo de mensaje gazebo\_msgs/LinkState, estable el estado de pose o giro de una articulación.
		\subitem \textbf{$\sim$/set\_model\_state}: Utiliza el tipo de mensaje gazebo\_msgs/ModelState, establece el estado de pose o giro de un modelo.
	\item Parámetros publicados de Gazebo
		\subitem \textbf{/use\_sim\_time}: Tipo de dato bool, notifica a ROS para que utilice el tópico /clock para manejar el tiempo de simulación.
	\item Temas publicados de Gazebo
		\subitem \textbf{/clock}: Utiliza el tipo de mensaje rosgraph\_msgs/Clock, publica el tiempo de simulación y es utilizado en conjunto con el parámetro /use\_sim\_time.
		\subitem \textbf{$\sim$/link\_states}: Utiliza el tipo de mensaje gazebo\_msgs/LinkStates, publica el estado de todas las articulaciones de la simulación. 
		\subitem \textbf{$\sim$/model\_states}: Utiliza el tipo de mensaje gazebo\_msgs/ModelStates, publica el estado de todos los modelos en la simulación.
	\item Servicios de Gazebo
		\subitem \textbf{Crear y destruir modelos en la simulación}: Estos servicios permiten al usuario generar y destruir modelos dentro la simulación.
		\subitem \textbf{Captadores de estado y propiedades}: Estos servicios permiten al usuario recuperar información de estado y propiedades sobre la simulación, así como de los objetos que se encuentran dentro de la misma.
		\subitem \textbf{Establecedores de estado y propiedades}: Estos servicios permiten al usuario establecer información de estados y propiedades sobre la simulación, así como los objetos que se encuentran dentro de la misma.
		\subitem \textbf{Control de la simulación}: Estos servicios permiten al usuario pausar y reaunudar la física dentro de la simulación.
		\subitem \textbf{Control de fuerza}: Estos servicios permiten al usuario aplicar fuerzas a cuerpos y articulaciones en la simulación.
\end{itemize}

\section{El robot MP-500}

Para la realización de este trabajo se utilizó el robot MP-500 del fabricante de robots móviles y sistemas robóticos Neobotix. Esta plataforma cuenta con soporte para ROS y sus paquetes se encuentran disponible en \cite{neobotix3}. Aparte de tener soporte para ROS también dispone para el usuario un modelo de simulación en Gazebo, como el que se muestra en la figura \ref{F:mp_500_sim}. Además, como muestra la figura \ref{F:medidas}, el diámetro del robot es de 814 mm, el diámetro de sus ruedas es de 260 mm y la separación entre sus ruedas es de 507 mm. 

\begin{figure}[H]
\centering
\includegraphics[width=0.35\textwidth]{./imagenes/neo_mp_500_simulation.png} 
\caption{Robot diferencial MP-500 dentro de la simulación. \cite{neobotix2}}
\label{F:mp_500_sim}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{./imagenes/mp500_medidas.png} 
\caption{Medidas del robot diferencial MP-500. \cite{neobotix1}}
\label{F:medidas}
\end{figure}

El robot MP-500 incluye difentes tipos de sensores incluidos en el paquete neo\_simulation de Neobotix; en este trabajo se utiliza el sensor láser S300 de Sick Sensor Intelligence, el cual se muestra en la figura \ref{F:sensor}. Cuenta con un rango de escaneo de $270^{\circ}$, un alcance del campo de 2$m$ a 3$m$ además es fácilmente instalable en una gran cantidad de máquinas debido a su diseño compacto. Este sensor es que se utiliza para la implementación de los algoritmos de evasión de obstáculos; al utilizar este sensor, la plataforma simulada guarda similitud a la plataforma física disponible en el CERLab, ya que ambas son configuraciones diferenciales que disponen de un sensor láser para su funcionamiento.
 
\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{./imagenes/sensorS300.png} 
\caption{Sensor láser S300 de Sick Sensor Intelligence. \cite{S300}}
\label{F:sensor}
\end{figure}