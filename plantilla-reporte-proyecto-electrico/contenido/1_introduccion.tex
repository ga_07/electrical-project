% ----------------------
  \chapter{Introducción}
% ----------------------
\label{C:introduccion}

Actualmente, la robótica es una disciplina que involucra la interacción de diferentes áreas de las ciencias aplicadas como la ingeniería eléctrica y electrónica, la ingeniería mecánica, la ingeniería de control, ciencias de la computación, entre otras, para crear algunos de los elementos de la automatización tales como vehículos autónomos guiados, robots de inspección, robots de limpieza e inclusive para sistemas de entretenimiento. El uso de estas máquinas puede ir desde aplicaciones industriales, pasando por el área médica, labores de rescate, hasta aplicaciones espaciales y militares \cite{WorldRobots, LandVehicles}. 
Dentro del campo de la robótica existe una clasificación para distinguir los diferentes tipos de robot según en el espacio en el que se trasladen; robots acuáticos si se desplazan debajo del agua, robots aéreos si vuelan por el aire y robots terrestres si se mueven sobre el suelo. Estos últimos, a su vez, vuelven a subdividirse en categorías según el tipo de extremidad que utilicen para trasladarse, ya sean patas, base de tracción o ruedas, y deben cumplir un mínimo de requerimientos para poder ser considerados autónomos; poseer la habilidad de mantener un sentido de su posición y navegar a través de un entorno sin intervención humana \cite{InDoorVehicles, AutonomousMobileRobots}. 

Este proyecto pretende utilizar un robot en configuración diferencial, la cual consiste en un cuerpo rígido con dos ruedas que no sufren deformaciones y son impulsadas por dos motores \cite{profeLeo}, como base para simular un robot autónomo móvil que pueda movilizarse sin necesidad de ayuda humana en un entorno plano.

\section{Alcances del proyecto}

El presente trabajo posee como base la utilización de un software de simulación para prueba de algoritmos en el área de robótica móvil; por esta razón su estudio y el aprendizaje de su uso es indespensable para este trabajo. De igual manera, se ahondará en la descripción del problema de navegación de una plataforma robótica móvil dentro de un ambiente horizontal cerrado, mediante la explicación teórica de algoritmos que le permitan a un robot seguir una trayectoria dada y evadir obstáculos. Para la ejecución de las simulaciones y pruebas de algoritmos, se tomará únicamente en cuenta la utilización de plataformas en configuración diferencial, por lo que queda fuera del alcance del proyecto la simulación de cualquier otro tipo de configuración. De igual manera, al ser un trabajo simulado, la implementación en las plataformas ya construidas en semestres anteriores, no formarán parte del tema de investigación. Asimismo, quedan fuera del alcance del proyecto la descripción teórica del problema de sintonizar los parámetros del controlador para los motores DC del robot, especificar su funcionamiento y la identificación de su modelo, ya que estas actividades forman parte del problema de la confección física de la plataforma.

\section{Justificación}

En el Laboratorio de Investigación en Ingeniería de Control de la Escuela de Ingeniería Eléctrica de la Universidad de Costa Rica, mejor conocido como CERLab (Control Engineering Research Laboratory), se encuentra en marcha una investigación en el área de la robótica móvil cooperativa. Dentro de esta investigación surge la necesidad de la utilización de plataformas móviles para llevar a cabo la prueba y validación de los algoritmos de dicho estudio. El laboratorio posee a su disposición varias de ellas, sin embargo, un problema presente en la investigación es que, al requerir un uso especializado, las y los asistentes involucrados en el proyecto deben realizar múltiples estudios previos con el fin de manipular y utilizar las plataformas. Por esta razón se busca una forma de simular las plataformas móviles disponibles en el laboratorio para que los y las presentes y futuros asistentes puedan familiarizarse con ellas de forma paralela con el estudio previo. El presente trabajo de investigación tiene como finalidad el implementar y simular algoritmos de seguimiento de trayectoria y evasión de obstáculos para una plataforma móvil que se utilizará en el estudio previamente mencionado, además de establecer las bases del manejo de herramientas de software de simulación robótica para futuras implementaciones de algoritmos y sus respectivas pruebas antes de ser cargados a una plataforma física.

\section{Objetivo general}

Simular a través de la utilización de software el funcionamiento de un robot diferencial de tal forma que posea movimiento autónomo al navegar en un entorno horizontal plano sin deslizamiento, mediante un algoritmo que lo dote con la capacidad de seguir una trayectoria dada y que a su vez pueda ser capaz de evitar obstáculos que se encuentren en el ambiente que lo rodee.

\section{Objetivos específicos}

\begin{itemize}
\item Realizar un análisis e investigación acerca del software de simulación robótica Gazebo y su integración con ROS (Robot Operating System), para utilizarlo como herramienta para ejecutar las respectivas pruebas para el proyecto.
\item Implementar dos algoritmos en un lenguaje de programación de alto nivel para lograr que la plataforma, simulada sobre un plano horizontal, adquiera la capacidad de seguir un trayectoria predefinida dentro de un entorno cerrado.
\item Verificar el correcto funcionamiento de algoritmos de seguimiento de trayectoria, a través de simulaciones realizadas en el software de simulación robótica Gazebo y el cálculo de índices de desempeño para el mismo.
\item Implementar un algoritmo en un lenguaje de programación de alto nivel para dotar a la plataforma, simulada sobre un plano horizontal, con la capacidad de evadir obstáculos estáticos dentro de un ambiente cerrado.
\item Validar el correcto funcionamiento de los algoritmos de evasión de obstáculos, mediante simulaciones realizadas en el software de simulación robótica Gazebo y el cáculo de índices de desempeño para el mismo.
\end{itemize}

\section{Metodología}

El presente proyecto se desarrollará mediante la ejecución de las siguientes actividades:

\begin{enumerate}
\item \textbf{Investigación bibliográfica:} Se llevará a cabo una investigación teórica acerca de los conocimientos introductorios del campo de la robótica, así como del software utilizado para simulación de robots. Asimismo, se estudiaŕa el resultado de investigaciones anteriores en las áreas de seguimiento de trayectorias así como evasión de obstáculos para la implementación de los algoritmos para la simulación de la plataforma.
\item \textbf{Selección de los algoritmos:} Se explorarán los distintos algoritmos para seguimiento de una trayectoria dada y de evasión de obstáculos disponibles en la literatura, para seleccionar dos del primer campo, y uno del segundo, utilizando como guía el desempeño que obtuvieron en trabajos anteriores.
\item \textbf{Implementación de los algoritmos:} Se implementarán dos algoritmos de seguimiento de una trayectoria dada y otros dos para evasión de obstáculos y así realizar un estudio comparativo, utilizando como base el lenguaje de programación Python 3 para aprovechar el uso de bibliotecas de software libre externas. De igual forma, se utilizarán bibliotecas pertenecientes a ROS (Robot Operative System), las cuales se utilizan para trabajar robótica de software libre.
\item \textbf{Simulaciones:} Se realizarán simulaciones de los algoritmos implementados para una plataforma en configuración diferencial, utilizando el software de simulación robótica Gazebo. De igual manera, durante la ejecución de las simulaciones se recolectarán datos para el estudio del desempeño de los algortimos sobre la plataforma simulada.
\item \textbf{Análisis:} Se realizará un análisis de los resultados obtenidos a partir de las simulaciones, de las cuales se utilizarán los datos recolectados para calcular el índice de desempeño de cada algoritmo y determinar su viabilidad para ser probados sobre una plataforma física.
\end{enumerate}

\section{Organización del documento}

En la sección \ref{C:antecedentes} de este trabajo se establecerán las bases teóricas sobre la robótica móvil, haciendo énfasis sobre la configuración diferencial, ya que es la utilizada en este trabajo. Se presenta la explicación teórica de los algoritmos para seguimiento de trayectoria y evasión de obstáculos implemetados en este trabajo, así como antecedentes en este campo.

En la sección \ref{C:desarrollo} se presenta una explicación de las herramientas de software utilizadas para la realización de este trabajo; se exponen los conceptos principales de ROS, Gazebo, la integración entre ambos y la plataforma utilizada.

En la sección \ref{C:Algoritmos} se muestran el diseño de los algoritmos, así como detalles de su implementación y su uso en conjunto con las herramientas utilizadas.

En sección \ref{C:Simulación y resultados} se desarrollan las simulaciones para los algoritmos implementados; se presentarán los datos recolectados como resultados y se realizará análisis sobre el desempeño y eficacia de cada uno de los métodos para determinar su viabilidad para ser probados en una plataforma física.

Por último, en la sección \ref{C:conclusiones} se presentan las conclusiones obtenidas a partir de la realización del proyecto, así como recomendaciones para futuros trabajos en la línea de simulación de plataformas robóticas móviles.
