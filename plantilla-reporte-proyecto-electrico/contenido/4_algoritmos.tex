% --------------------
  \chapter{Algoritmos implementados}
% --------------------
\label{C:Algoritmos}

\section{Generalidades}

Para la realización de este proyecto se decidió realizar la implementación de los algoritmos en el lenguaje de programación de alto nivel Python \footnote{Para realización de este trabajo se utilizó la versión Python 2.7.17 debido a la versión y distribución de ROS utilizadas. A la fecha de realización de este documento existe dos versiones de ROS; ROS y ROS 2, cada una cuenta con diferentes distribuciones. Se utilizó la versión de ROS en conjunto con la distribución ROS Melodic Morenia.}. A continuación se muestran distintas razones que aportaron peso a la toma de esta decisión:

\begin{itemize}
	\item \textbf{Facilidad y velocidad a la hora de crear prototipos}: Al ser Python un lenguaje de programación de alto nivel interpretado permite una gran flexibilidad a la hora de escribir los programas; además, no requiere manejo explícito de memoria como otros lenguajes compilados. Asimismo, posee a disposición un shell interactivo con el que se pueden realizar pruebas y está ampliamente documentado. Estos factores resultan en una aceleración considerable en el proceso de desarrollo, a la vez que genera una reducción de la cantidad de líneas de código necesarias. En comparación con un lenguaje compilado como C++, se puede obervar una reducción de horas de trabajo y líneas de código hasta la mitad del trabajo. Esto último es un factor de importancia, dado el ajustado cronograma que posee el proyecto eléctrico.
	\item \textbf{Compatibilidad con ROS}: Python cuenta con una API oficial para ROS llamada \emph{Rospy}; esta biblioteca cliente habilita a los programadores a interactuar de forma rápida con los tópicos, servicios y parámetros de ROS. Al mismo tiempo, disminuye el tiempo de preparación, lo que adelanta las pruebas para los algoritmos implementados. La importancia de esta biblioteca cliente es tal que muchas de las herramientas de ROS como \emph{rostopic} y \emph{rosservice} están construidas sobre \emph{Rospy}. Actualmente sigue recibiendo mantenimiento.
	\item \textbf{Compatibilidad con el software de simulación}: El software de simulación que se utilizó para validar los algoritmos cuenta con integración directa con ROS; este último a su vez posee la API para trabajar en Python.
	\item \textbf{Disponibilidad de paquetes externos}: Python dispone de una extensa lista de paquetes y bibliotecas para procesamiento de datos, cálculos matemáticos y aplicaciones científicas. Para la implementación de los algoritmos los paquetes \emph{NumPy}, \emph{Pandas} y \emph{tf (Python)} fueron ampliamente utilizados.
	\end{itemize}
	
El código de cada algoritmo implementado se adjuntará comentado al final del documento.

\section{Algoritmos de seguimiento de trayectoria}

\subsection{Punto descentralizado}

La implementación para el algoritmo de punto descentralizado se basó en las ecuaciones expliacadas en la sección \ref{sec:pt_descentralizado}. La tabla \ref{psedo:pt_desc} muestra un resumen del funcionamiento del algoritmo implementado; el flujo del programa sigue el mismo flujo que el algoritmo de control.

\begin{table}[H]
\begin{center}
\begin{tabular}{l}
\toprule
\textbf{Entradas:} Coordenadas $X_{ref}$, $Y_{ref}$ \\
\textbf{Salidas:} Odemetría $x, y, \theta$\\
\textbf{Parámetros:} Distancia $e$ al punto, contantes del control proporcional $k_{px}, k_{py}$\\
\midrule
\begin{lstlisting}[language=Python]
def odometry_control():
	Obtener los valores globales de X y Ys a partir de la ecuacion (2.19)
	Obtener las velocidades vxp y vyp de las ruedas en los ejes globales a partir de la ecuacion (2.21)
	return vxp, vyp
	
def inverse_kinematic_model(vxp, vyp):
	Obtener las velocidades de las ruedas en los ejes locales vr y vl, utilizando vxp y vyp, a partir de la ecuacion (2.22)
	return vr, vl

def direct_kinematics():
	Obtener los valores de odometria para el robot a partir de la ecuacion (2.3)
	
def cmd_vel_publisher():
	Llamar a la funcion odometry_control() y obtener vxp, vyp
	Llamar a la funcion inverse_kinematic_model(vxp, vyp) y pasarle las velocidades como parametros, se obtienen vr y vl
	Llamar a la funcion direct_kinematics()
	Indicarle las velocidades vr y vl a las ruedas
	
def run():
	while El codigo se ejecute:
		Llamar a la funcion cmd_vel_publisher()
\end{lstlisting} \\
\bottomrule
\end{tabular}
\end{center}
\caption{Implementación del algoritmo de punto descentralizado}
\label{psedo:pt_desc}
\end{table}

Para ejecutar la simulación se necesitan los métodos de las clases que se muestran en la figura \ref{class:pt_desc}. La primera es la clase de point\_pub, que se encarga de publicar las coordenadas $(x, y)$ para el punto de referencia del robot. Esta clase puede ser sustituida por un homólogo que calcule varios puntos de una trayectoria predeterminada que deba seguir el robot. La segunda clase corresponde a tf\_point y se encarga de aplicar a los puntos de referencia los cálculos de transformadas de marco de referencia explicados en la sección \ref{frames}. La última clase corresponde a decentralized\_point, y se encarga del controlador necesario para ejecutar el algoritmo. La documentación más detallada de cada clase y sus métodos se encuentra en la sección de anexos.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{./imagenes/class_decentralized.png} 
\caption{Diagrama de clases de la implementación del algoritmo de punto descentralizado.}
\label{class:pt_desc}
\end{figure}

Como este algoritmo se implementó con la biblioteca cliente Rospy, la implementación cuenta con una serie de nodos y tópicos, conceptos de ROS explicados en la sección \ref{ros}. En la figura \ref{nodes:pt_desc} se muestra un diagrama con los principales nodos y tópicos, donde los rectángulos corresponden a estos últimos y los óvalos a los primeros, respectivamente.

Los nodos de más importancia son:

\begin{itemize}
	\item \textbf{/point\_pub}: Publica las coordenadas de los puntos de referencia en el tópico /desired\_point.
	\item \textbf{/tf\_point}: Se suscribe al tópico /desired\_point para obtener las coordenadas a las que se les debe aplicar la transformación de marco de referencia.
	\item \textbf{/decentralized\_point\_node}: Controlador que implementa el algoritmo de punto descentralizado.
	\item \textbf{/gazebo}: Herramienta de simulación robótica Gazebo integrada en ROS.
\end{itemize}

Los tópicos de más importancia son:

\begin{itemize}
	\item \textbf{/desired\_point}: Contiene las coordenadas del punto de referencia.
	\item \textbf{/tf}: Contiene las coordenadas del punto de referencia transformadas de un marco a otro.
	\item \textbf{/cmd\_vel}: Contiene las velocidades del robot dentro de la simulación.
	\item \textbf{/odom}: Contiene los valores de odometría del modelo de la simulación, necesarios para compararlos con la odometría calculada por el algoritmo.
	\item \textbf{/joint\_states}: Contiene el desplazamiento angular de las ruedas, necesario para calcular la velocidad lineal de las ruedas en el ciclo de control anterior.
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{./imagenes/pt_dec_node.png} 
\caption{Diagrama de nodos y tópicos de la implementación del algoritmo de punto descentralizado.}
\label{nodes:pt_desc}
\end{figure}

\subsection{Persecución pura} 

Para implementar el algoritmo de persecución pura se utilizan las ecuaciones de la sección \ref{sec:persecucion_pura}. La tabla \ref{psedo:p_p} muestra un resumen del funcionamiento del algoritmo implementado.

\begin{table}[]
\begin{center}
\begin{tabular}{l}
\toprule
\textbf{Entradas:} Coordenadas $X_{ref}$, $Y_{ref}$ \\
\textbf{Salidas:} Velocidades $V_{d}, V_{i}$ \\
\textbf{Parámetros:} Velocidad lineal deseada $v$ \\
\midrule
\begin{lstlisting}[language=Python]
def pure_persuit_control():
	Obtener las velocidades del robot a partir de las ecuaciones (2.9), (2.16), (2.15), (2.17) y (2.18)
	return vd, vi
	
def inverse_kinematic_model(vd, vi):
	Obtener las velocidades de las ruedas en los ejes locales vr y vl, utilizando vd y vi, a partir de la ecuacion (2.22)
	return vr, vl

def direct_kinematics():
	Obtener los valores de odometria para el robot a partir de la ecuacion (2.3)
	
def cmd_vel_publisher():
	Llamar a la funcion pure_persuit_control() y obtener vd, vi
	Llamar a la funcion inverse_kinematic_model(vd, vi) y pasarle las velocidades como parametros, se obtienen vr y vl
	Llamar a la funcion direct_kinematics()
	Indicarle las velocidades vr y vl a las ruedas
	
def run():
	while El codigo se ejecute:
		Llamar a la funcion cmd_vel_publisher()
\end{lstlisting} \\
\bottomrule
\end{tabular}
\end{center}
\caption{Implementación del algoritmo de persecución pura}
\label{psedo:p_p}
\end{table}

Para ejecutar la simulación se necesitan los métodos de las clases que se muestran en la figura \ref{class:p_p}. 

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{./imagenes/class_pure_pursuit.png} 
\caption{Diagrama de clases de la implementación del algoritmo de persecución pura.}
\label{class:p_p}
\end{figure}

La clase de point\_pub se encarga de publicar las coordenadas $(x, y)$ para el punto de referencia del robot, también puede ser sustituida por una clase que calcule los puntos de una trayectoria a seguir. La segunda clase corresponde a tf\_point, es la encargada de aplicar la transformación de un marco de referencia a otro, mediante las ecuaciones explicadas en la sección \ref{frames}, a los puntos de referencia. La última clase corresponde a pure\_pursuit, implementa el controlador para el algoritmo de persecución pura. La documentación más detallada de cada clase y sus métodos se encuentra en la sección de anexos.

Para la implementación se utilizó la API de ROS para Python; se utilizan los conceptos de ROS explicados en la sección \ref{ros}, los tópicos y nodos en la simulación. En la figura \ref{nodes:p_p} se muestra un diagrama con los principales nodos y tópicos, donde los rectángulos corresponden a estos últimos y los óvalos a los primeros, respectivamente.

Los nodos de más importancia son:

\begin{itemize}
	\item \textbf{/point\_pub}: Publica las coordenadas de los puntos de referencia en el tópico /desired\_point.
	\item \textbf{/tf\_point}: Se suscribe al tópico /desired\_point para obtener las coordenadas a las que se les debe aplicar la transformación de marco de referencia.
	\item \textbf{/pure\_pursuit}: Controlador que implementa el algoritmo de persecución pura.
	\item \textbf{/gazebo}: Herramienta de simulación robótica Gazebo integrada en ROS.
\end{itemize}

Los tópicos de más importancia corresponden a:

\begin{itemize}
	\item \textbf{/desired\_point}: Contiene las coordenadas del punto de referencia.
	\item \textbf{/tf}: Contiene las coordenadas del punto de referencia transformadas de un marco de referencia a otro.
	\item \textbf{/cmd\_vel}: Contiene las velocidades del robot dentro de la simulación.
	\item \textbf{/odom}: Contiene los valores de odometría del modelo de la simulación, necesarios para compararlos con la odometría calculada por el algoritmo.
	\item \textbf{/joint\_states}: Contiene el desplazamiento angular de las ruedas, necesario para calcular la velocidad lineal de las mismas en el ciclo de control anterior.
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{./imagenes/pure_pursuit_node.png} 
\caption{Diagrama de nodos y tópicos de la implementación del algoritmo de persecución pura.}
\label{nodes:p_p}
\end{figure}

\section{Algoritmos de evasión de obstáculos}

\subsection{Vehículo de Braintenberg 2a: Miedo}

La implementación para el algoritmo de Braintenberg 2a se basó en la teoría expliacada en la sección \ref{sec:braintenberg}. La tabla \ref{psedo:braintenberg} muestra un resumen del funcionamiento algoritmo implementado; el flujo del programa sigue el mismo flujo que el algoritmo de evasión de obstáculos. 

Este algoritmo, a diferencia de los mostrados previamente en este capítulo, cuenta con una única clase que se encarga de tomar la lectura de los sensores, y utilizarlas para ver si debe aumentar, disminuir o mantener la velocidad de las ruedas del robot. También posee los métodos para el modelo cinemático y el modelo cinemático inverso para un robot en configuración diferencial dados por las ecuaciones (\ref{diff3}) y (\ref{vel_PD_sin_v}) respectivamente. La documentación más detallada de la clase y sus métodos se encuentra en la sección de anexos.

Como este algoritmo se implementó con la biblioteca cliente Rospy, la implementación cuenta con una serie de nodos y tópicos, conceptos de ROS explicados en la sección \ref{ros}. En la figura \ref{nodes:braintenberg} se muestra un diagrama con los principales nodos y tópicos, donde los rectángulos corresponden a estos últimos y los óvalos a los primeros, respectivamente.

\begin{table}[H]
\begin{center}
\begin{tabular}{l}
\toprule
\textbf{Entradas:} Lectura de los sensores \\
\textbf{Salidas:} Velocidades $V_{d}, V_{i}$ \\
\textbf{Parámetros:} Velocidad lineal deseada $v$ \\
\midrule
\begin{lstlisting}[language=Python]
Obtener las lecturas del sensor
	
def inverse_kinematic_model(vxp, vyp):
	Obtener las velocidades de las ruedas en los ejes locales vr y vl, utilizando vxp y vyp, a partir de la ecuacion (2.22)
	return vr, vl

def direct_kinematics():
	Obtener los valores de odometria para el robot a partir de la ecuacion (2.3)
	
def cmd_vel_publisher():
	if la lectura del sensor aumenta:
		Aumentar velocidad de la rueda (vxp y vyp)
	else:
		Mantener velocidad (vxp y vyp)
	Llamar a la funcion inverse_kinematic_model(vxp, vyp) y pasarle las velocidades como parametros, se obtienen vr y vl
	Llamar a la funcion direct_kinematics()
	Indicarle las velocidades vr y vl a las ruedas
	
def run():
	while El codigo se ejecute:
		Llamar a la funcion cmd_vel_publisher()
\end{lstlisting} \\
\bottomrule
\end{tabular}
\end{center}
\caption{Implementación del algoritmo de Braintenber}
\label{psedo:braintenberg}
\end{table}

Los nodos de más importancia son:

\begin{itemize}
	\item \textbf{/braintenberg\_node}: Contiene el algoritmo de evasión de obstáculos, publica las velocidades en el tópico /cmd\_vel. Se sucribe al tópico de /lidar\_1/scan\_filter para obtener las lecturas del sensor.
	\item \textbf{/gazebo}: Herramienta de simulación robótica Gazebo integrada en ROS; en este caso publica en el tópico /lidar\_1/scan\_filter las lecturas del sensor.
\end{itemize}

Los tópicos de más importancia son:

\begin{itemize}
	\item \textbf{/cmd\_vel}: Contiene las velocidades del robot dentro de la simulación, las cuales el nodo de Gazebo escuchará por ser suscriptor, y utilizará dentro de la misma.
	\item \textbf{/lidar\_1/scan\_filter}: Contiene las lecturas del sensor dentro de la simulación. El nodo de Gazebo es el encargado de publicar dichas lecturas dentro de este tópico.
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.72\textwidth]{./imagenes/braintenberg_node.png} 
\caption{Diagrama de nodos y tópicos de la implementación del algoritmo de Braintneberg 2a.}
\label{nodes:braintenberg}
\end{figure}

\begin{comment}

\subsection{VFH+}

Para la implementación del algoritmo VFH+ se utilizan las ecuaciones de la sección \ref{sec:vfh_plus}. La tabla \ref{psedo:vfh_plus} muestra un resumen del funcionamiento algortimo implementado.

Al igual que la implementación del otro algoritmo de evasión de obstáculos, este también utiliza una única clase. Esta se encarga de recolectar los datos provenientes de los sensores e implementar cada una de las secciones del algoritmo.

Como este algoritmo se implementó con la biblioteca cliente Rospy, la implementación cuenta con una serie de nodos y tópicos, conceptos de ROS explicados en la sección \ref{ros}. En la figura \ref{nodes:vfh_plus} se muestra un diagrama con los principales nodos y tópicos, donde los rectángulos corresponden a estos últimos y los óvalos a los primeros, respectivamente.

Los nodos de más importancia son:

\begin{itemize}
	\item \textbf{/vfh\_plus\_node}: Contienen el algoritmo de evasión de obstáculos VFH+, publica las velocidades en el tópico /cmd\_vel. Se sucribe al tópico de /lidar\_1/scan\_filter para obtener las lecturas del sensor.
	\item \textbf{/gazebo}: Herramienta de simulación robótica Gazebo integrada en ROS; en este caso publica en el tópico /lidar\_1/scan\_filter las lecturas del sensor.
\end{itemize}

Los tópicos de más importancia son:

\begin{itemize}
	\item \textbf{/cmd\_vel}: Contiene las velocidades del robot dentro de la simulación, las cuales el nodo de Gazebo escuchará por ser suscriptor, y utilizará en la simulación
	\item \textbf{/lidar\_1/scan\_filter}: Contiene las lecturas del sensor dentro de la simulación. El nodo de Gazebo es el encargado de publicar dichas lecturas dentro de este tópico.
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.72\textwidth]{./imagenes/vfh_node.png} 
\caption{Diagrama de nodos y tópicos de la implementación del algoritmo VFH+.}
\label{nodes:vfh_plus}
\end{figure}

\begin{table}[H]
\begin{center}
\begin{tabular}{l}
\toprule
\textbf{Entradas:} Lectura de los sensores \\
\textbf{Salidas:} Velocidades $V_{d}, V_{i}$ \\
\textbf{Parámetros:} Velocidad mínima y máxima del robot \\
\midrule
\begin{lstlisting}[language=Python]
Obtener las lecturas del sensor
	
def update_obstacle_density():
	Actualiza la densidad de obstaculos a partir de la lectura de los sensores	
	
def update_active_window():
	Actualiza la ventana activa a partir de las ecuaciones (2.23) y (2.24)

def update_polar_histogram():
	Actualiza el histograma polar a partir de las ecuaciones (2.35) y (2.26)

def update_bin_polar_histogram():
	Actualiza el histograma polar binario a partir de la ecuacion (2.37)

def update_masked_polar_hist():
	Actualiza el histograma polar binario a partir de la ecuacion (2.44) y la tabla 2.1

def find_valleys(): 
	Encuentra los valles a partir del histograma polar mascarado
	
def calculate_steering_dir():
	Determina las ecuaciones candidatas y su costo a partir de las ecuaciones (2.45), (2.46), (2.47), (2.48) y (2.49)

def calcute_speed():
	Calcula la velodidad de las ruedas.
	
def cmd_vel_publisher():
	Llamar a la funcion update_active_window()
	Llamar a la funcion update_polar_histogram()
	Llamar a la funcion update_bin_polar_histogram()
	Llamar a la funcion update_masked_polar_hist()
	Llamar a la funcion find_valleys()
	Llamar a la funcion calculate_steering_dir()
	Llamar a la funcion calcute_speed()
	
def run():
	while El codigo se ejecute:
		Llamar a la funcion cmd_vel_publisher()
\end{lstlisting} \\
\bottomrule
\end{tabular}
\end{center}
\caption{Implementación del algoritmo de VFH+}
\label{psedo:vfh_plus}
\end{table}

\end{comment}

