#!/usr/bin/env python
  
import rospy
import tf

class listener_tf_point():

    def __init__(self):

        rospy.init_node("listener_tf_test")
        self.tf_x = 0
        self.tf_y = 0
        self.tf_listener = tf.TransformListener()

    def run_listener(self):
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            try:
                (trans,rot) = self.tf_listener.lookupTransform("odom", "point", rospy.Time(0))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
            self.tf_x = trans[0]
            self.tf_y = trans[1]
            print("**********")
            print("self.tf_x: ", self.tf_x)
            print("self.tf_y: ", self.tf_y)
            print("**********")
            print(" ")
            rate.sleep()

def main():
    listener_tf = listener_tf_point()
    listener_tf.run_listener()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass
