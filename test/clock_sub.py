#!/usr/bin/env python

import rospy
from rosgraph_msgs.msg import Clock

def clock_callback(msg):
    time = msg.clock.to_sec()
    print("Current simulation time: ", time)

def clock_listener():
    rospy.init_node("clk_subscriber", anonymous=False)
    rospy.Subscriber("clock", Clock, clock_callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        clock_listener()
    except rospy.ROSInterruptException:
        pass