#!/usr/bin/env python

import rospy
from gazebo_msgs.msg import ModelStates

def vel_callback(msg):
    print("***************")
    print("model state twist x: ", msg.twist[11].linear.x)
    print("model state twist y: ", msg.twist[11].linear.y)
    print("***************")

def vel_listener():
    rospy.init_node("vel_sub", anonymous=False)
    rospy.Subscriber("gazebo/model_states", ModelStates, vel_callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        vel_listener()
    except rospy.ROSInterruptException:
        pass
