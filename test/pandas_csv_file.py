import numpy as np
import pandas as pd

class file_writing():

    def __init__(self):
        self.Lk = np.zeros(3)
        self.my_Lk = np.zeros(3)
        self.str1 = "Lk x"
        self.str2 = "My Lk x"
        self.str3 = "Lk y"
        self.str4 = "My Lk y"
        self.str5 = "Lk theta"
        self.str6 = "My Lk theta"
        self.data_list = list()

    def randomize(self):
        self.Lk = np.random.rand(3)
        self.my_Lk = np.random.rand(3)

    def write(self):
        counter = 0
        while counter < 15:
            results = [str(self.Lk[0]), str(self.my_Lk[0]), str(self.Lk[1]), str(self.my_Lk[1]), str(self.Lk[2]), str(self.my_Lk[2])]
            self.data_list.append(results)
            self.randomize()
            counter += 1
        frame = pd.DataFrame(self.data_list, columns=[self.str1, self.str2, self.str3, self.str4, self.str5, self.str6])
        print(frame)
        frame.to_csv('data.csv')

def main():
    file_test = file_writing()
    file_test.write()
if __name__ == "__main__":
    main()