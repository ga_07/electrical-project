#!/usr/bin/env python
import rospy
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist, Pose2D
from sensor_msgs.msg import JointState
from rosgraph_msgs.msg import Clock


global tk
tk=0
global tk_1
tk_1=0
global Xd_1
Xd_1=0
global Xi_1
Xi_1=0

def clock_callback(msg):
    """
    This is the callback function, which gets the simulation time
    """
    global tk
    t = msg.clock.to_sec()
    tk=t
    return t

def set_up_time():
    """
    This function calculates the sample time period each loop and update it:

    Ts = Sample period
    tk = Actual time sample
    tk-1 = Previous time sample

    Ts = tk - (tk-1)
    """
    global tk
    global tk_1
    Ts = tk - tk_1
    tk_1 = tk
    return Ts

def callback(data):
    Xd = data.position[0]
    Xi = data.position[1]
    global Xd_1
    global Xi_1
    global Ts
    if Xd == 0:
        Xd=Xd_1
    if Xi==0:
        Xi=Xi_1
    R = 0.13
    Ts=set_up_time()
    if Ts == 0.000 or Ts > 2:
        Ts=0.01
    Vd=(Xd-Xd_1)*R/Ts
    Vi=(Xi-Xi_1)*R/Ts
    Xd_1=Xd
    Xi_1=Xi

    c=0.551
    V=(Vd+Vi)/2
    w=(Vd-Vi)/c
    rospy.loginfo_throttle(2,rospy.get_caller_id() + ' Ts= %s', Ts)
    rospy.loginfo_throttle(2,rospy.get_caller_id() + ' V= %s', V)
    rospy.loginfo_throttle(2,rospy.get_caller_id() + ' w= %s', w)



def listener():

    rospy.init_node('listener', anonymous=True)


    tk=rospy.Subscriber("clock", Clock,clock_callback, queue_size=100)
    rospy.Subscriber("joint_states",JointState,callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
