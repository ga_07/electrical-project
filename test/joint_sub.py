#!/usr/bin/env python

import rospy
import numpy as np
from sensor_msgs.msg import JointState
from rosgraph_msgs.msg import Clock

class joint_and_velocity():

    def __init__(self):
        rospy.init_node("joint_subscriber", anonymous=False)
        rospy.Subscriber("joint_states", JointState, self.joint_callback)
        self.x_n = 0
        self.x_n_1 = 0
        self.wxk_1 = 0
        self.y_n = 0
        self.y_n_1 = 0
        self.wyk_1 = 0
        self.vxk_1 = 0
        self.vyk_1 = 0
        self.R = 0.13
        self.Ts = 0.01

    def joint_callback(self, msg):
        if((msg.position[0] != 0) and (msg.position[1] != 0)):
            self.x_n = msg.position[0]
            self.y_n = msg.position[1]
        self.wxk_1 = (self.x_n - self.x_n_1)/self.Ts
        self.wyk_1 = (self.y_n - self.y_n_1)/self.Ts
        self.vxk_1 = self.R * self.wxk_1
        self.vyk_1 = self.R * self.wyk_1
        self.x_n_1 = self.x_n
        self.y_n_1 = self.y_n

    def vel_pos(self):
        print("self.vxk_1: ", self.vxk_1)
        print("self.vyk_1: ", self.vyk_1)

    def run(self):
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            print("**********")
            self.vel_pos()
            rate.sleep()
            print("**********")
            print(" ")

def main():
    joint = joint_and_velocity()
    joint.run()
if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass