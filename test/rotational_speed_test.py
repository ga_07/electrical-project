#!/usr/bin/env python
# coding=utf-8

import rospy
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
import tf

class rotational_speed():

    def __init__(self):

        rospy.init_node('rotational_speed_test', anonymous=False)
        self.error_rate = 0.05
        self.desired_V = 0.5
        self.b = 0.507
        self.Lk = np.zeros(3)  
        self.Xref_Yref = np.zeros(2)
        self.cmd_vel_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=100)
        self.odom_vel_sub = rospy.Subscriber("odom", Odometry, self.odom_callback, queue_size=100)
        self.tf_listener = tf.TransformListener()

    def odom_callback(self, msg):
        orientation_list = [msg.pose.pose.orientation.x, msg.pose.pose.orientation.y,
                            msg.pose.pose.orientation.z, msg.pose.pose.orientation.w]
        (roll, pitch, theta) = tf.transformations.euler_from_quaternion(orientation_list)
        self.Lk[0] = msg.pose.pose.position.x
        self.Lk[1] = msg.pose.pose.position.y
        self.Lk[2] = theta

    def angle_wrap(self, angle):
        while (angle < -np.pi):
            angle += 2 * np.pi
        while (angle > np.pi):
            angle -=  2 * np.pi
        return angle

    def rotate(self):
        delta_x = self.Xref_Yref[0] - self.Lk[0]
        delta_y = self.Xref_Yref[1] - self.Lk[1]
        angle_to_the_target = np.arctan2(delta_y, delta_x)
        if (np.absolute(self.Lk[2] - angle_to_the_target) > self.error_rate):
            vd = self.desired_V 
            vi = 0 
        else:
            vd = 0
            vi = 0
        vk = np.array([vd, vi])
        return vk

    def cmd_vel_publisher(self):
        msg = Twist()
        vk_wk_1 = self.rotate()
        wk = (vk_wk_1[0] - vk_wk_1[1])/self.b
        msg.linear.x = vk_wk_1[0]
        msg.linear.y = vk_wk_1[1]
        msg.linear.z = 0.0
        msg.angular.x = 0.0
        msg.angular.y = 0.0
        msg.angular.z = wk
        self.cmd_vel_pub.publish(msg)

    def run(self):
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            try:
                (trans,rot) = self.tf_listener.lookupTransform("odom", "point", rospy.Time(0))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
            self.Xref_Yref[0] = trans[0]
            self.Xref_Yref[1] = trans[1]
            self.cmd_vel_publisher()
            rate.sleep()

def main():
    test = rotational_speed()
    test.run()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass