#!/usr/bin/env python
# coding=utf-8

import rospy
from sensor_msgs.msg import LaserScan

def lidar_callback(msg):
    angle_min = msg.angle_min        
    angle_max = msg.angle_max        
    angle_increment = msg.angle_increment
    range_min = msg.range_min 
    range_max = msg.range_max
    ranges = msg.ranges
    intensities = msg.intensities
    print("angle_min: ", angle_min)
    print("angle_max: ", angle_max)
    print("angle_increment: ", angle_increment)
    print("range_min: ", range_min)
    print("range_max: ", range_max)
    print("ranges lengh: ", len(ranges))
    print("intensities lengh: ", len(intensities))
    right_sensor = 0
    left_sensor = 0
    infinity = float('inf') 
    half = (len(ranges)/2)
    for i in range(len(ranges)):
        if (i < half):
            if (ranges[i] != infinity):
                left_sensor += ranges[i]   
        else:
            if (ranges[i] != infinity):
                right_sensor += ranges[i]
    right_sensor = float(right_sensor/half)
    left_sensor = float(left_sensor/half) 
    print("right_sensor: ", right_sensor)  
    print("left_sensor: ", left_sensor)
    print(" ")

def lidar_listener():
    rospy.init_node('lidar_listener', anonymous=False)
    lidar_sub = rospy.Subscriber("lidar_1/scan_filtered", LaserScan, lidar_callback, queue_size=100)
    rospy.spin()

if __name__ == '__main__':
    try:
        lidar_listener()
    except rospy.ROSInterruptException:
        pass