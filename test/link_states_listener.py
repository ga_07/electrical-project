#!/usr/bin/env python

import rospy
from gazebo_msgs.msg import LinkState

class LinkStates_listener():

    def __init__(self):

        rospy.init_node("link_state_listener", anonymous=False)
        self.vx = 0.1
        self.vy = 0.1
        link_states_sub = rospy.Subscriber("gazebo/link_states", LinkState, self.link_callback,  queue_size=100)

    def link_callback(self, msg):
        self.vx = msg.twist.linear.x
        self.vy = msg.twist.linear.y

    def run(self):
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            print("self.vx: ", self.vx)
            print("self.vy: ", self.vy)
            print(" ")
            rate.sleep()

def main():
    listener = LinkStates_listener()
    listener.run()
if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass