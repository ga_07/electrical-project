import numpy as np
import matplotlib.pyplot as plt

class Proportional_position_values():

    def __init__(self):
        """Constants"""
        self.Ts = 0.01
        self.kpx = 0.95
        self.kpy = 0.95
        self.c = 0.2535
        self.wk_1 = 0.0
        self.e = self.c * 2.5
        """Lists"""
        self.x_pos = list()
        self.y_pos = list()
        self.final_result = list()
        """Arrays"""
        self.xp_yp = np.zeros(2)    
        self.Lk = np.zeros(3) 
        self.Lk_1 = np.zeros(3)
        self.Vx_Vy_k1 = np.zeros(2)
        self.vd_vi = np.zeros(2)
        self.Xref_Yref = np.zeros(2)
        self.x_range = np.zeros(2)
        self.y_range = np.zeros(2)
        self.A = np.array([
                [self.kpx, 0],
                [0, self.kpy]
            ])
        """Tuples"""
        self.tuple_result = ()

    def odometry_control(self):
        self.xp_yp = np.array([
            self.Lk[0] + self.e * np.cos(self.Lk[2]),
            self.Lk[1] + self.e * np.sin(self.Lk[2])
        ])
        B = np.subtract(self.Xref_Yref, self.xp_yp)
        vxp_vyp = np.dot(self.A, B)
        return vxp_vyp

    def inverse_kinematic_model(self):
        vxp_vyp = self.odometry_control()
        E = np.array([
            [(self.e * np.cos(self.Lk[2])) + (0.5 * self.c * np.sin(self.Lk[2])),
             (self.e * np.sin(self.Lk[2])) - (0.5 * self.c * np.cos(self.Lk[2]))],
            [(self.e * np.cos(self.Lk[2])) - (0.5 * self.c * np.sin(self.Lk[2])),
             (self.e * np.sin(self.Lk[2])) + (0.5 * self.c * np.cos(self.Lk[2]))]
        ])
        inverse_e = float(1/self.e)
        F = inverse_e * E
        vd_vi = np.dot(F, vxp_vyp)
        return vd_vi

    def kinematic_model(self):
        self.Vx_Vy_k1 = self.vd_vi
        self.vd_vi = self.inverse_kinematic_model()
        vk_1 = float((self.Vx_Vy_k1[0] + self.Vx_Vy_k1[1])/2) 
        self.wk_1 = float((self.Vx_Vy_k1[0] - self.Vx_Vy_k1[1])/self.c)
        self.Lk_1 = self.Lk
        G = np.array([
            vk_1 * np.cos(self.Lk_1[2]),
            vk_1 * np.sin(self.Lk_1[2]),
            self.wk_1
        ])
        H = self.Ts * G
        self.Lk = np.add(self.Lk_1, H)
        self.x_pos.append(self.Lk[0].astype(float))
        self.y_pos.append(self.Lk[1].astype(float))

    def set_reference(self, Xref, Yref):
        self.Xref_Yref[0] = Xref
        self.Xref_Yref[1] = Yref
        self.x_range[0] = self.Xref_Yref[0] - (self.Xref_Yref[0] * 0.1)
        self.x_range[1] = self.Xref_Yref[0] + (self.Xref_Yref[0] * 0.1) 
        self.y_range[0] = self.Xref_Yref[1] - (self.Xref_Yref[1] * 0.1)
        self.y_range[1] = self.Xref_Yref[1] + (self.Xref_Yref[1] * 0.1)

    def plot_result(self, fig_number):
        self.tuple_result = (self.kpx, self.kpy, fig_number)
        self.final_result.append(self.tuple_result)
        fig_name = "Figure_" + str(fig_number)
        plt.figure()
        plt.plot(self.x_pos, self.y_pos)
        plt.xlabel("Robot's x position")
        plt.ylabel("Robot's y position")
        plt.title("Odometry result")
        plt.grid(True)
        plt.savefig(fig_name)

    def check_result(self):
        flag = False
        if ((self.Lk[0] > self.x_range[0]) and (self.Lk[0] < self.x_range[1]) and (self.Lk[1] > self.y_range[0]) and (self.Lk[0] < self.y_range[1])):
            flag = True
        else:
            flag = False
        return flag

    def printing_result(self):
        for i in range(len(self.final_result)):
            print(" ")
            print("**********")
            print("Simulation " + str(i))
            print("kpx value: ", self.final_result[i][0])
            print("kpy value: ", self.final_result[i][1])
            print("Figure number " + str(self.final_result[i][2]))
            print("**********")
            print(" ")

def main():
    test = Proportional_position_values()
    test.set_reference(3.5, 3.5)
    flag = False
    while flag != True:
        test.kinematic_model()
        flag = test.check_result()
    test.plot_result(0)
    test.printing_result()
if __name__ == "__main__":
    main()