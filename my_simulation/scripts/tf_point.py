#!/usr/bin/env python
  
import rospy
import tf
from geometry_msgs.msg import Pose2D

class transform_point():

    def __init__(self):

        rospy.init_node("tf_point", anonymous=False)
        """X coordinate of the point to transform"""
        self.x_tf = 0
        """X coordinate of the point to transform"""
        self.y_tf = 0
        """Point subscriber"""
        self.point_sub = rospy.Subscriber("desired_point", Pose2D, self.point_callback, queue_size=100)

    def tf_broadcaster(self):
        """
        This function takes a given point and performs its transformation to a global
        reference frame
        """
        rate = rospy.Rate(100)
        br = tf.TransformBroadcaster()
        while not rospy.is_shutdown():
            """
            This method sends the point tranformation to the global refrence frame
            """
            br.sendTransform((self.x_tf, self.y_tf, 0),
                         tf.transformations.quaternion_from_euler(0, 0, 1.0),
                         rospy.Time.now(),
                         "point",
                         "odom")
            rate.sleep()

    def point_callback(self, msg):
        """
        This is the callback function, which gets the desired point
        """
        self.x_tf = msg.x
        self.y_tf = msg.y

def main():
    tf = transform_point()
    tf.tf_broadcaster()
    rospy.spin()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass