#!/usr/bin/env python
# coding=utf-8
  
"""Paquetes importados"""
import rospy
import numpy as np
from geometry_msgs.msg import Pose2D

class infinite_test():

    """
    Clase utilizada para implementar la prueba de un infinito para algoritmos de
    seguimiento de trayectoria.

    Atributos:
    ---------
    rospy.init_node('square_test_node', anonymous=False): Instancia del objeto init_node de Rospy
        Inicia el nodo con su respectivo nombre.
    point_pub: Instancia del objeto Publisher de Rospy
        Objeto que publica el punto a seguir en su respectivo tópico.

    Métodos:
    -------
    __init__:
        Constructor de la clase.
    infinite:
        Ejecuta la prueba.
    point_publisher:
        Publica los puntos a seguir.
    """

    def __init__(self):
        """
        Inicializa los atributos de la clase

        Parámetros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        rospy.init_node('infinite_test_node', anonymous=False)
        self.point_pub = rospy.Publisher("desired_point", Pose2D, queue_size=10) 

    def infinite(self):
        """
        Ejecuta la prueba

        Parámetros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        while not rospy.is_shutdown():
            seconds = rospy.get_time()
            d = 6
            t = 0.25 * seconds
            x = d * np.sqrt(2) * np.cos(t) / (np.power(np.sin(t), 2) + 1)
            y = d * np.sqrt(2) * np.cos(t) * np.sin(t) / (np.power(np.sin(t), 2) + 1)
            self.point_publisher(x,y)
    
    def point_publisher(self, x, y):
        """
        Publica los puntos

        Parámetros:
        ----------
            x: float
                Coordenada x del punto 
            y: float
                Coordenada y del punto
        Returns:
        --------
            Ninguno
        """
        msg = Pose2D()
        msg.x = x
        msg.y = y
        self.point_pub.publish(msg)

def main():
    test = infinite_test()
    test.infinite()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass