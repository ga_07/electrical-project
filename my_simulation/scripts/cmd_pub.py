#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist

def cmd_vel_publisher_test():
    pub_test = rospy.Publisher("cmd_vel", Twist, queue_size=10)
    rospy.init_node("cmd_vel_pub_test", anonymous=False)
    msg = Twist()
    msg.linear.x = 2.1
    msg.linear.y = 1.2
    rate = rospy.Rate(60)
    while not rospy.is_shutdown():
        pub_test.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        cmd_vel_publisher_test()
    except rospy.ROSInterruptException:
        pass