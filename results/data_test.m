close all;
clear all;

name0 = 'p_p_square_stddev0_0_.csv';
name1 = 'p_p_square_stddev0_5_.csv';
name2 = 'p_p_square_stddev1_5_.csv';
track = 'square_test.csv';

start0 = 1;
start1 = 1;
start2 = 1;
start3 = 8;

M0 = readmatrix(name0);
Lk_x0 = M0(:,2);
Lk_y0 = M0(:,3);
Lk_theta0 = M0(:,4);
t0 = M0(:,5);
max_time = size(Lk_x0);

M1 = readmatrix(name1);
Lk_x1 = M1(:,2);
Lk_y1 = M1(:,3);
Lk_theta1 = M1(:,4);
max_time1 = size(Lk_x1);
t1 = M1(:,5);

M2 = readmatrix(name2);
Lk_x2 = M2(:,2);
Lk_y2 = M2(:,3);
Lk_theta2 = M2(:,4);
max_time2 = size(Lk_x2);
t2 = M2(:,5);

Track = readmatrix(track);
track_x = Track(:,2);
track_y = Track(:,3);

Title1 = "Posición (x,y) del robot";
Title2 = "Posición angular del robot";    

figure(1);
plot(Lk_x0(start0:end), Lk_y0(start0:end), '-.r', 'LineWidth', 2);
title(Title1);
xlabel("Posición x del robot");
ylabel("Posición y del robot");
hold on;
plot(Lk_x1(start1:end), Lk_y1(start1:end), '--b', 'LineWidth', 2);
plot(Lk_x2(start2:end), Lk_y2(start2:end), '-.g', 'LineWidth', 2);
plot(track_x(start3:end), track_y(start3:end), 'LineWidth', 1);
hold off;
legend("Desv. 0.0", "Desv. 0.5", "Desv. 1.5", "Trayectoria cuadrada");
grid on;

figure(2);
polarplot(Lk_theta0(start0:end), '-.r', 'LineWidth', 2);
title(Title2);
hold on;
polarplot(Lk_theta1(start1:end), '--b', 'LineWidth', 2);
polarplot(Lk_theta2(start2:end), '-.g', 'LineWidth', 2);
hold off;
legend("Desv. 0.0", "Desv. 0.5", "Desv. 1.5");
grid on;

%T0 = t0(7594)-t0(3);
%fprintf('t0 = %f\n', T0);
%T1 = t1(7715)-t1(3);
%fprintf('t1 = %f\n', T1);
%T2 = t2(6320)-t2(3);
%fprintf('t2 = %f\n', T2);

error = 0;
ex0 = zeros(1,length(Lk_x0));
ex1 = zeros(1,length(Lk_x0));
ex2 = zeros(1,length(Lk_x0));

for i = 2:615
    error = track_x(i) - Lk_x0(i);
    ex0(i) = error;
    error = track_x(i) - Lk_x1(i);
    ex1(i) = error;
    error = track_x(i) - Lk_x2(i);
    ex2(i) = error;
end

IAEx0 = sum(abs(ex0));
IAEx1 = sum(abs(ex1));
IAEx2 = sum(abs(ex2));

fprintf('IAEx0: %f\n',IAEx0);
fprintf('IAEx1: %f\n',IAEx1);
fprintf('IAEx2: %f\n',IAEx2);

error = 0;
ey0 = zeros(1,length(Lk_y0));
ey1 = zeros(1,length(Lk_y0));
ey2 = zeros(1,length(Lk_y0));

for i = 1:615
    error = track_y(i) - Lk_y0(i);
    ey0(i) = error;
    error = track_y(i) - Lk_y1(i);
    ey1(i) = error;
    error = track_y(i) - Lk_y2(i);
    ey2(i) = error;
end

IAEy0 = sum(abs(ey0));
IAEy1 = sum(abs(ey1));
IAEy2 = sum(abs(ey2));

fprintf('IAEy0: %f\n',IAEy0);
fprintf('IAEy1: %f\n',IAEy1);
fprintf('IAEy2: %f\n',IAEy2);

d0 = zeros(1,length(Lk_x0));
d1 = zeros(1,length(Lk_x1));
d2 = zeros(1,length(Lk_x2));

for i = 1:615
    d0(i) = sqrt((track_x(i)-Lk_x0(i))^2 + (track_y(i)-Lk_y0(i))^2);
    d1(i) = sqrt((track_x(i)-Lk_x1(i))^2 + (track_y(i)-Lk_y1(i))^2);
    d2(i) = sqrt((track_x(i)-Lk_x2(i))^2 + (track_y(i)-Lk_y2(i))^2);
end

IAEd0 = sum(abs(d0));
IAEd1 = sum(abs(d1));
IAEd2 = sum(abs(d2));

fprintf('IAEd0: %f\n',IAEd0);
fprintf('IAEd1: %f\n',IAEd1);
fprintf('IAEd2: %f\n',IAEd2);