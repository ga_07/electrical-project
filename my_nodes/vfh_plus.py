#!/usr/bin/env python
# coding=utf-8

import tf
import rospy
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
# from rosgraph_msgs.msg import Clock
# from sensor_msgs.msg import JointState

GRID_SIZE = 125
C_MAX = 20
RESOLUTION = 0.04
WINDOW_SIZE = 25
WINDOW_CENTER = WINDOW_SIZE/2
ALPHA = 5
HIST_SIZE = 360/ALPHA
D_MAX2 = np.sqrt((WINDOW_SIZE-1)*RESOLUTION)/2
CTE_B = 10
CTE_A = 1 + CTE_B*D_MAX2
ROBOT_R = 0.814
D_S = 0.04
R_RS = ROBOT_R + D_S
T_LOW = 3000
T_HIGH = 3500
WIDE_V = HIST_SIZE/8
V_MAX = 0.5
V_MIN = 0.0
MU1 = 6
MU2 = 2
MU3 = 2
MAX_COST = 180*(MU1 + MU2 + MU3)
MAG = 0
BETA = 1
DIST2 = 2
A_B_DIST = 3
GAMMA = 4
DIST_b = 0.507
DIST_c = DIST_b/2
X_POS = 0
Y_POS = 1
THETA = 2

class VFH_plus():

    def __init__(self):
        rospy.init_node('vfh_plus_node', anonymous=False)
        grid_dim = (GRID_SIZE, GRID_SIZE)
        self.obstacle_grid = np.zeros(grid_dim, dtype = np.int8)
        window_dim = (WINDOW_SIZE, WINDOW_SIZE, 5)
        self.active_window = np.zeros(window_dim)
        for i in range(WINDOW_SIZE):
            for j in range(WINDOW_SIZE):
                if (j == WINDOW_CENTER and i == WINDOW_CENTER):
                    continue
                beta_p = np.degrees(np.arctan2(j - WINDOW_CENTER, i - WINDOW_CENTER))
                if (beta_p < 0):
                    self.active_window[i, j, BETA] = beta_p + 360
                else:
                    self.active_window[i, j, BETA] = beta_p
                dist2 = np.square(RESOLUTION) * np.float(np.square(i - WINDOW_CENTER) + np.square(j - WINDOW_CENTER))
                self.active_window[i, j, DIST2] = dist2
                self.active_window[i, j, A_B_DIST] = CTE_A - CTE_B*dist2
                self.active_window[i, j, GAMMA] = np.degrees(np.arcsin(R_RS/np.sqrt(dist2)))
                if np.isnan(self.active_window[i, j, GAMMA]):
                    self.active_window[i, j, GAMMA] = np.float(90)
        self.polar_hist = np.zeros(HIST_SIZE)
        self.bin_polar_hist = np.zeros(HIST_SIZE)
        self.masked_polar_hist = np.zeros(HIST_SIZE)
        self.valleys = []
        self.Lk = np.zeros(3)
        self.i_o = 0
        self.j_o = 0
        self.k_o = 0
        self.target = np.zeros(2)
        self.prev_dir = 0
        self.prev_cost = 0
        self.cmd_vel_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=100)
        self.odom_sub = rospy.Subscriber("odom", Odometry, self.odom_callback, queue_size=100)
        self.lidar_sub = rospy.Subscriber("lidar_1/scan_filtered", LaserScan, self.update_obstacle_density, queue_size=100)
        self.tf_listener = tf.TransformListener()

    def odom_callback(self, msg):
        orientation_list = [msg.pose.pose.orientation.x, msg.pose.pose.orientation.y,
                            msg.pose.pose.orientation.z, msg.pose.pose.orientation.w]
        (roll, pitch, theta) = tf.transformations.euler_from_quaternion(orientation_list)
        self.Lk[X_POS] = msg.pose.pose.position.x
        self.Lk[Y_POS] = msg.pose.pose.position.y
        self.Lk[THETA] = theta

    def update_obstacle_density(self, msg):
        angle = msg.angle_min             
        angle_incr = msg.angle_increment
        infinity = float('inf')
        ranges = msg.ranges
        for k in ranges:
            if (ranges[k] == infinity or ranges[k] == 0):
                angle += angle_incr
                continue
            i = int((self.Lk[X_POS] + ranges[i] * np.cos(angle + self.Lk[THETA]))/RESOLUTION)
            j = int((self.Lk[Y_POS] + ranges[i] * np.cos(angle + self.Lk[THETA]))/RESOLUTION)
            if (self.obstacle_grid[i, j] < C_MAX):
                self.obstacle_grid[i, j] += 1
            angle += angle_incr

"""
    def active_grid(self):
        i_window = max(self.i_o - WINDOW_CENTER, 0)
        j_window = max(self.j_o - WINDOW_CENTER, 0)
        i_max = min(i_window + WINDOW_CENTER, GRID_SIZE)
        j_max = min(j_window + WINDOW_CENTER, GRID_SIZE)
        return self.obstacle_grid[i_window:i_max, j_window:j_max]
"""

    def update_active_window(self):
        i_window = self.i_o - WINDOW_CENTER
        j_window = self.j_o - WINDOW_CENTER
        for i in range(WINDOW_SIZE):
            for j in range(WINDOW_SIZE):
                if (j == WINDOW_CENTER and i == WINDOW_CENTER):
                    continue
                grid_i = i_window + i
                grid_j = j_window + j
                if (grid_i < 0 or grid_i > GRID_SIZE or grid_j < 0 or grid_j > GRID_SIZE):
                    self.active_window[i, j, MAG] = 0.0
                else:
                    cij = float(self.obstacle_grid[grid_i, grid_j])
                    self.active_window[i, j, MAG] = np.square(cij) * self.active_window[i, j, A_B_DIST]

    def update_polar_histogram(self):
        for i in range(HIST_SIZE):
            self.polar_hist[i] = 0
        for i in range(WINDOW_SIZE):
            for j in range(WINDOW_SIZE):
                if (j == WINDOW_CENTER and i == WINDOW_CENTER):
                    continue
                beta = self.active_window[i, j, BETA]
                gamma = self.active_window[i, j, GAMMA]
                low = int(np.ceil((self.active_window[i, j , BETA] - self.active_window[i, j, GAMMA])/ALPHA))
                high = int(np.floor((self.active_window[i, j, BETA] + self.active_window[i, j, GAMMA])/ALPHA))
                k_range = np.linspace(low, high, high - low + 1, True, dtype = int)
                for x in k_range:
                    k_range[x] = x % HIST_SIZE
                for k in k_range:
                    self.polar_hist[k] += self.active_window[i, j, MAG]
            
    def update_bin_polar_histogram(self):
        for i in range(HIST_SIZE):
            if (self.polar_hist[i] > T_HIGH):
                blocked = True
            else if (self.polar_hist[i] < T_LOW):
                blocked = False
            else:
                self.polar_hist[i] = blocked

    def update_masked_polar_hist(self):
        phi_back = np.degrees(self.Lk[THETA]) + 180.0
        if (phi_back > 360):
            phi_back = phi_back - 360
        phi_left = phi_back
        phi_right = phi_back
        X_r = (WINDOW_SIZE * RESOLUTION)/2
        Y_r = (WINDOW_SIZE * RESOLUTION)/2
        for i in range(WINDOW_SIZE):
            for j in range(WINDOW_SIZE):
                if (self.active_window[i, j , MAG] <= 2*(C_MAX**2) or (i == WINDOW_CENTER and j == WINDOW_CENTER)):
                    continue
                if (self.active_window[i, j, BETA] == np.degrees(self.Lk[THETA])):
                    cij_x = (i + 0.5) * RESOLUTION
                    cij_y = (j + 0.5) * RESOLUTION
                    r_robL_x = ROBOT_R * np.cos(self.Lk[THETA] + np.radians(90.0))
                    r_robL_y = ROBOT_R * np.sin(self.Lk[THETA] + np.radians(90.0))
                    r_steer_x = X_r + r_robL_x
                    r_steer_y = Y_r + r_robL_y
                    c_dist2 = np.square(cij_x - r_steer_x) + np.square(cij_y - r_steer_y)
                    if (c_dist2 < np.square(R_RS + ROBOT_R)):
                        phi_left = self.active_window[i, j, BETA] + 0.1
                    r_robR_x = ROBOT_R * np.cos(self.Lk[THETA] - np.radians(90.0))
                    r_robR_y = ROBOT_R * np.sin(self.Lk[THETA] - np.radians(90.0))
                    r_steer_x = X_r + r_robR_x
                    r_steer_y = Y_r + r_robR_y
                    c_dist2 = np.square(cij_x - r_steer_x) + np.square(cij_y - r_steer_y)
                    if (c_dist2 < np.square(R_RS + ROBOT_R)):
                        phi_left = self.active_window[i, j, BETA] - 0.1
                else if (self.is_in_range(np.degrees(self.Lk[THETA]), phi_left, self.active_window[i, j, BETA])):
                    cij_x = (i + 0.5) * RESOLUTION
                    cij_y = (j + 0.5) * RESOLUTION
                    r_robL_x = ROBOT_R * np.cos(self.Lk[THETA] + np.radians(90.0))
                    r_robL_y = ROBOT_R * np.sin(self.Lk[THETA] + np.radians(90.0))
                    r_steer_x = X_r + r_robL_x
                    r_steer_y = Y_r + r_robL_y
                    c_dist2 = np.square(cij_x - r_steer_x) + np.square(cij_y - r_steer_y)
                    if (c_dist2 < np.square(R_RS + ROBOT_R)):
                        phi_left = self.active_window[i, j, BETA] 
                else if (self.is_in_range(np.degrees(self.Lk[THETA]), phi_right, self.active_window[i, j, BETA])):
                    r_robR_x = ROBOT_R * np.cos(self.Lk[THETA] - np.radians(90.0))
                    r_robR_y = ROBOT_R * np.sin(self.Lk[THETA] - np.radians(90.0))
                    r_steer_x = X_r + r_robR_x
                    r_steer_y = Y_r + r_robR_y
                    c_dist2 = np.square(cij_x - r_steer_x) + np.square(cij_y - r_steer_y)
                    if (c_dist2 < np.square(R_RS + ROBOT_R)):
                        phi_right = self.active_window[i, j, BETA] 
        for k in range(HIST_SIZE):
            if (self.bin_polar_hist[k] == False and self.is_in_range(phi_right, phi_left, k*ALPHA)):
                self.masked_polar_hist[k] = False
            else:
                self.masked_polar_hist[k] = True

    def is_in_range(start, end, angle):
        if (start < end):
            return (start <= angle and angle <= end) 
        else:
            return (start <= angle and angle <= 360) or (0 <= angle and angle <= end)

    def find_valleys(self):
        start = None
        for i in range(HIST_SIZE):
            if (self.masked_polar_hist[i]):
                start = i
                break
        if (start == None):
            return -1
        self.valleys = []
        valley_found = False
        for j in range(HIST_SIZE+1):
            index = (start + j) % HIST_SIZE
            if not valley_found:
                if not self.masked_polar_hist[index]:
                    v_start = index
                    valley_found = True
            else: 
                if (self.masked_polar_hist[index]):
                    self.valleys.append(tuple([v_start, index-1]))
                    valley_found = False
        return len(self.valleys)

    def calculate_steering_dir(self, valley_count):
        t_dir = None
        if (self.target != None):
            t_dir = np.degrees(np.arctan2(self.target[1] - self.Lk[Y_POS], self.target[0] - self.Lk[X_POS]))
            if (t_dir < 0):
                t_dir += 360
        else:
            t_dir = np.degrees(self.Lk[THETA])
        candidate_dirs = []
        if (valley_count == -1):
            candidate_dirs.append(t_dir)
        else:
            for i in self.valleys:
                s1, s2 = v
                if (s2 >= s1):
                    v_size = s2 - s1
                else:
                    v_size = HIST_SIZE - (s1 - s2)
                if (v_size < WIDE_V):
                    c_center = ALPHA * (s1 + v_size/2)
                    if (c_center >= 360):
                        c_center -= 360
                    candidate_dirs.append(c_center)
                else:
                    c_right = ALPHA * (s1 + WIDE_V/2)
                    if (c_right >= ):
                        c_right -= 360
                    c_left = ALPHA * (s2 + WIDE_V/2)
                    if (c_left < 0):
                        c_left += 360
                    candidate_dirs.append(c_right)
                    candidate_dirs.append(c_left)
                    if (c_right != c_left and self.is_in_range(c_right, c_left, t_dir)):
                        candidate_dirs.append(t_dir)
        new_dir = None
        best_cost = None
        for k in candidate_dirs:
            cost = MU1 * self.absolute_angle(c, t_dir) + MU2 * self.absolute_angle(c, np.degrees(self.Lk[THETA])) + MU3 * self.absolute_angle(c, self.prev_cost)
            if (best_cost == None):
                new_dir = c
                best_cost = cost
            else if (cos < best_cost):
                new_dir = c
                best_cost = cost
        self.prev_dir = new_dir
        self.prev_cost = best_cost

    def calcute_speed(self):
        V = V_MAX * (1 - self.prev_cost/MAX_COST) + V_MIN
        return V

    def absolute_angle(self, a1, a2):
        return min(360 - abs(a1 - a2), abs(a1 - a2))

    def cmd_vel_publisher(self):
        """
        This function publishes the calculated wheels velocities during the loop
        """
        self.update_active_window()
        self.update_polar_histogram()
        self.update_bin_polar_histogram()
        self.update_masked_polar_hist()
        self.find_valleys()
        self.calculate_steering_dir()
        V = self.calcute_speed()
        msg = Twist()
        msg.linear.x = V
        msg.linear.y = V
        msg.linear.z = 0.0
        msg.angular.x = 0.0
        msg.angular.y = 0.0
        msg.angular.z = 0.0
        self.cmd_vel_pub.publish(msg)

    def run(self):
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            try:
                (trans,rot) = self.tf_listener.lookupTransform("odom", "point", rospy.Time(0))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
            self.target[0] = trans[0]
            self.target[1] = trans[1]
            self.i_o = int(self.target[0]/RESOLUTION)
            self.j_o = int(self.target[2]/RESOLUTION)
            self.k_o = int(self.Lk[THETA_POS]/ALPHA) % HIST_SIZE
            self.cmd_vel_publisher()
            rate.sleep()

def main():
    node = VFH_plus()
    node.run()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass
