#!/usr/bin/env python
# coding=utf-8
  
"""Paquetes importados"""
import rospy
from geometry_msgs.msg import Pose2D

"""Tiempo durante el que se va a publicar el punto"""
RUN_TIME = 0.2
"""Inicio del cuadrado"""
START = 2.0
"""Fin del cuadrado"""
END = 5.0
"""Tamaño del salto"""
STEP = 0.1
"""Total de saltos por lado"""
TOTAL = int((END-1.0)/STEP)
"""Indice de x"""
X_POS = 0
"""Indice de y"""
Y_POS = 1

class square_test():

    """
    Clase utilizada para implementar la prueba de un cuadrado para algoritmos de
    seguimiento de trayectoria.

    Atributos:
    ---------
    rospy.init_node('square_test_node', anonymous=False): Instancia del objeto init_node de Rospy
        Inicia el nodo con su respectivo nombre.
    point_pub: Instancia del objeto Publisher de Rospy
        Objeto que publica el punto a seguir en su respectivo tópico.

    Métodos:
    -------
SLEEP_TIME = 0.01
    __init__:
        Constructor de la clase.
    track:
        Calcula los puntos de la trayectoria.
    square:
        Ejecuta la prueba.
    point_publisher:
        Publica los puntos a seguir.
    """

    def __init__(self):
        """
        Inicializa los atributos de la clase

        Parámetros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        rospy.init_node('square_test_node', anonymous=False)
        self.point_pub = rospy.Publisher("desired_point", Pose2D, queue_size=10)  

    def track(self):
        """
        Calcula los puntos de la trayectoria

        Parámetros:
        ----------
            Ninguno
        Returns:
        --------
            track: Python list
                Lista que contiene las coordenadas (x,y) de la trayectoria.
        """
        track = []
        my_tuple = tuple()
        my_tuple = (END-START, END-START)
        track.append(my_tuple)
        x = END-START
        y = END-START
        
        for i in range(TOTAL):
          if (x < END):
            x += STEP
          else:
            y += STEP
          my_tuple = (x,y)
          track.append(my_tuple)

        for j in range(TOTAL):
            x -= STEP
            my_tuple = (x, y)
            track.append(my_tuple)

        for k in range(TOTAL):
            y -= STEP
            my_tuple = (x, y)
            track.append(my_tuple)  

        for l in range(TOTAL):
            x += STEP
            my_tuple = (x, y)
            track.append(my_tuple)

        for m in range(TOTAL/2):
            y += STEP
            my_tuple = (x, y)
            track.append(my_tuple)

        return track

    def square(self):
        """
        Ejecuta la prueba

        Parámetros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        track = self.track()
        for i in range(len(track)):
            start = rospy.Time.now()
            while (rospy.Time.now() - start).to_sec() < RUN_TIME:
                self.point_publisher(track[i][X_POS], track[i][Y_POS])
    
    def point_publisher(self, x, y):
        """
        Publica los puntos

        Parámetros:
        ----------
            x: float
                Coordenada x del punto 
            y: float
                Coordenada y del punto
        Returns:
        --------
            Ninguno
        """
        msg = Pose2D()
        msg.x = x
        msg.y = y
        self.point_pub.publish(msg)

def main():
    test = square_test()
    test.square()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass
        

    