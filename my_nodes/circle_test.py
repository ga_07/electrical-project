#!/usr/bin/env python
# coding=utf-8
  
"""Paquetes importados"""
import rospy
import numpy as np
from geometry_msgs.msg import Pose2D

class circle_test():

    """
    Clase utilizada para implementar la prueba de un círculo para algoritmos de
    seguimiento de trayectoria.

    Atributos:
    ---------
    rospy.init_node('square_test_node', anonymous=False): Instancia del objeto init_node de Rospy
        Inicia el nodo con su respectivo nombre.
    point_pub: Instancia del objeto Publisher de Rospy
        Objeto que publica el punto a seguir en su respectivo tópico.

    Métodos:
    -------
    __init__:
        Constructor de la clase.
    circle:
        Ejecuta la prueba.
    point_publisher:
        Publica los puntos a seguir.
    """

    def __init__(self):
        """
        Inicializa los atributos de la clase

        Parámetros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        rospy.init_node('circle_test_node', anonymous=False)
        self.point_pub = rospy.Publisher("desired_point", Pose2D, queue_size=10) 

    def circle(self):
        """
        Ejecuta la prueba

        Parámetros:
        ----------
            Ninguno
        Returns:
        --------
            Ninguno
        """
        while not rospy.is_shutdown():
            seconds = rospy.get_time()
            t = 0.05 * seconds
            x = 4 * np.cos(2*np.pi*t)
            y = 4 * np.sin(2*np.pi*t)
            self.point_publisher(x,y)
    
    def point_publisher(self, x, y):
        """
        Publica los puntos

        Parámetros:
        ----------
            x: float
                Coordenada x del punto 
            y: float
                Coordenada y del punto
        Returns:
        --------
            Ninguno
        """
        msg = Pose2D()
        msg.x = x
        msg.y = y
        self.point_pub.publish(msg)

def main():
    test = circle_test()
    test.circle()
if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass