#!/usr/bin/env python
  
import rospy
from geometry_msgs.msg import Pose2D

def point_publisher():
    x = -5.0
    y = 5.0
    rospy.init_node("point_pub", anonymous=False)
    pub = rospy.Publisher("desired_point", Pose2D, queue_size=10)
    rate = rospy.Rate(100)
    msg = Pose2D()
    msg.x = x
    msg.y = y
    msg.theta = 0
    while not rospy.is_shutdown():
        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        point_publisher()
    except rospy.ROSInterruptException:
        pass
